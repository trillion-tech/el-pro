import { DataSource, DataSourceOptions } from 'typeorm';
import { database } from './config';

export const ormConfig: DataSourceOptions = database[database.default];

const datasource = new DataSource({
  ...ormConfig,
  migrations: ['src/migrations/**/*.{ts,js}'],
})

export default  datasource;