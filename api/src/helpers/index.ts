
import * as bcrypt from 'bcryptjs';

export function env (envConfig: string, defaultConfig: any = null) {
  return process.env[envConfig] ? process.env[envConfig] : defaultConfig;
};

export function hash (str: string): Promise<string> {
  return bcrypt.hash(str, env('SALT'));
};
