class Logger {
  log (...message) {
    console.log(...message);
  }

  warning(...message) {
    console.log('\x1b[33m%s\x1b[0m', ...message);
  }

  error(...message) {
    console.log('\x1b[31m%s\x1b[0m', ...message);
  }

  process(...message) {
    console.log('\x1b[34m%s\x1b[0m', ...message);
  }

  info(...message) {
    console.log(`\x1b[36m%s\x1b[0m`, ...message);
  }
}

export default Logger;