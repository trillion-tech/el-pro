import { User } from './user.entity';
import { database } from '../config';
import { createConnection } from 'typeorm';

const connection = async () => {
  const databaseType = database['default'];
  const databaseConfigs = database[databaseType];
  try {
    const connection = await createConnection({
      entities: [
        __dirname + "/*.ts"
      ],
      ...databaseConfigs,
      migrations: ['src/migrations/**/*.{ts,js}'],
      cli: {
        // entitiesDir: 'src/modules',
        migrationsDir: 'src/migrations',
      },
    });

    console.log('Connection Created');

    await connection.synchronize(false);

    console.log('Synchronized');
  } catch (err) {
    console.error(err);
  }
};

export default connection;
