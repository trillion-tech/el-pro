import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, DeleteDateColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
// https://orkhan.gitbook.io/typeorm/docs/active-record-data-mapper
// https://www.npmjs.com/package/typeorm
@Entity('users')
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'username',
    nullable: false,
    type: 'varchar',
  })
  username: string;

  @Column({ name: 'password', select: false, nullable: false, type: 'varchar' })
  password: string;
}
