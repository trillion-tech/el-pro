import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, DeleteDateColumn, JoinColumn, OneToOne } from 'typeorm';
import { BaseEntity} from './base.entity';
import { Lesson as LessonEntity } from './lesson.entity';
import { Admin as AdminEntity } from './admin.entity';

// https://orkhan.gitbook.io/typeorm/docs/active-record-data-mapper
// https://www.npmjs.com/package/typeorm
@Entity('levels')
export class Level extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'created_by',
    type: 'integer',
    nullable: false,
    comment: 'Admin Id',
  })
  createdBy: string;

  @Column({
    name: 'updated_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
  })
  updatedBy: string;

  @Column({
    name: 'deleted_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
  })
  deletedBy: string;

  @Column({
    name: 'name',
    nullable: false,
    type: 'varchar'
  })
  name: string;
  
  @Column({
    name: 'lesson_id',
    nullable: false,
    type: 'integer',
  })
  lessonId: number;

  // relationships
  @OneToOne(() => LessonEntity)
  @JoinColumn({ name: "lesson_id", referencedColumnName: "id", foreignKeyConstraintName: "levels_lesson_id_fk"})
  lesson: LessonEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'created_by', referencedColumnName: 'id', foreignKeyConstraintName: 'levels_created_by_fk' })
  creator: AdminEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'updated_by', referencedColumnName: 'id', foreignKeyConstraintName: 'levels_updated_by_fk' })
  updater: AdminEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'deleted_by', referencedColumnName: 'id', foreignKeyConstraintName: 'levels_deleted_by_fk' })
  eraser: AdminEntity;
}
