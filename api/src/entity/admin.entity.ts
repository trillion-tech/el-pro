import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, DeleteDateColumn, JoinColumn, OneToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
// https://orkhan.gitbook.io/typeorm/docs/active-record-data-mapper
// https://www.npmjs.com/package/typeorm
@Entity('admins')
export class Admin extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'created_by',
    type: 'integer',
    nullable: false,
    comment: 'Admin Id',
    default: 0
  })
  createdBy: string;

  @Column({
    name: 'updated_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
    default: 0
  })
  updatedBy: string;

  @Column({
    name: 'deleted_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
    default: 0
  })
  deletedBy: string;

  @Column({
    name: 'username',
    nullable: false,
    type: 'varchar',
  })
  username: string;

  @Column({ name: 'password', select: false, nullable: false, type: 'varchar' })
  password: string;

  @JoinColumn({ name: 'created_by', referencedColumnName: 'id', foreignKeyConstraintName: 'admins_created_by_fk' })
  creator: Admin;

  @OneToOne(() => Admin)
  @JoinColumn({ name: 'updated_by', referencedColumnName: 'id', foreignKeyConstraintName: 'admins_updated_by_fk' })
  updater: Admin;

  @OneToOne(() => Admin)
  @JoinColumn({ name: 'deleted_by', referencedColumnName: 'id', foreignKeyConstraintName: 'admins_deleted_by_fk' })
  eraser: Admin;
}
