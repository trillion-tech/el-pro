import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  UpdateDateColumn,
  CreateDateColumn,
  DeleteDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { BaseEntity } from './base.entity';
import { Admin as AdminEntity } from './admin.entity';

// https://orkhan.gitbook.io/typeorm/docs/active-record-data-mapper
// https://www.npmjs.com/package/typeorm
@Entity('lessons')
export class Lesson extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'created_by',
    type: 'integer',
    nullable: false,
    comment: 'Admin Id',
  })
  createdBy: string;

  @Column({
    name: 'updated_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
  })
  updatedBy: string;

  @Column({
    name: 'deleted_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
  })
  deletedBy: string;

  @Column({
    name: 'name',
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    name: 'content',
    type: 'mediumtext',
    nullable: false,
  })
  content: string;

  @Column({
    name: 'media_url',
    type: 'varchar',
    nullable: false,
  })
  mediaUrl: string;

  @Column({
    name: 'image_url',
    type: 'varchar',
    nullable: false,
  })
  imageUrl: string;

  @Column({
    name: 'time',
    type: 'integer',
    nullable: false,
    default: 0,
  })
  time: string;

  // relationships
  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'created_by', referencedColumnName: 'id', foreignKeyConstraintName: 'lessons_created_by_fk' })
  creator: AdminEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'updated_by', referencedColumnName: 'id', foreignKeyConstraintName: 'lessons_updated_by_fk' })
  updater: AdminEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'deleted_by', referencedColumnName: 'id', foreignKeyConstraintName: 'lessons_deleted_by_fk' })
  eraser: AdminEntity;
}
