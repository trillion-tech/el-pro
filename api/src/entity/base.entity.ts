import { BaseEntity as BaseEntityOrm, FindManyOptions, FindOptionsWhere, Entity, SaveOptions, CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm';
// https://orkhan.gitbook.io/typeorm/docs/active-record-data-mapper
// https://www.npmjs.com/package/typeorm

//

export type PaginationLengthView<Entity> = {
  total: number;
  page: number;
  data: Array<Entity>;
  perPage: number;
};

export class BaseEntity extends BaseEntityOrm {

  @CreateDateColumn({
    nullable: false,
    name: 'created_at',
  })
  createdAt: Date;

  @DeleteDateColumn({
    nullable: true,
    name: 'deleted_at',
  })
  deletedAt: Date;

  @UpdateDateColumn({
    nullable: true,
    name: 'updated_at',
  })
  updatedAt: Date;

  timestamps() {
    return false;
  }
  /**
   * Counts entities that match given WHERE conditions.
   */
  static async paginate<T extends BaseEntity>(
    this: { new (): T } & typeof BaseEntity,
    page?: number,
    perPage?: number,
    where?: FindOptionsWhere<T>,
  ): Promise<any> {
    const total: number = await this.getRepository<T>().countBy(where || {});

    let data: Array<any> = [];

    if (total > 0) {
      perPage = perPage || 15;
      page = page || 1;
      let skipIndex = 0;

      if (page - 1 > 0) {
        skipIndex = (page - 1) * perPage;
      }
      data = await this.getRepository<T>().find({
        where: where || {},
        skip: skipIndex,
        take: perPage,
      });
    }

    const dataView: PaginationLengthView<T> = {
      total,
      data,
      page: page || 1,
      perPage: perPage || 15,
    };

    return dataView;
  }

  /**
   * Update current entity in the database.
   * If entity does not exist in the database then inserts, otherwise updates.
   */
  update(options?: SaveOptions): Promise<this> {
    const baseEntity = this.constructor as typeof BaseEntity;
    return baseEntity.getRepository().save(this, options);
  }
}
