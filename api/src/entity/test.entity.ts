import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, DeleteDateColumn, OneToOne, JoinColumn } from 'typeorm';
import { BaseEntity} from './base.entity';
import { Lesson as LessonEntity } from './lesson.entity';
import { Level as LevelEntity } from './level.entity';
import { Admin as AdminEntity } from './admin.entity';

// https://orkhan.gitbook.io/typeorm/docs/active-record-data-mapper
// https://www.npmjs.com/package/typeorm
@Entity('tests')
export class Test extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'level_id',
    nullable: false,
    type: 'integer',
  })
  levelId: number;

  @Column({
    name: 'created_by',
    type: 'integer',
    nullable: false,
    comment: 'Admin Id',
  })
  createdBy: string;

  @Column({
    name: 'updated_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
  })
  updatedBy: string;

  @Column({
    name: 'deleted_by',
    type: 'integer',
    nullable: true,
    comment: 'Admin Id',
  })
  deletedBy: string;

  @Column({
    name: 'lesson_id',
    nullable: false,
    type: 'integer',
  })
  lessonId: number;

  @Column({
    name: 'content',
    nullable: false,
    type: 'varchar'
  })
  content: string;

  // relationships
  @OneToOne(() => LessonEntity)
  @JoinColumn({ name: "lesson_id", referencedColumnName: "id", foreignKeyConstraintName: "test_levels_level_id_foreign_key"})
  lesson: LessonEntity;

  @OneToOne(() => LevelEntity)
  @JoinColumn({ name: "level_id", referencedColumnName: "id", foreignKeyConstraintName: "test_levels_lesson_id_foreign_key"})
  level: LevelEntity;


  @JoinColumn({ name: 'created_by', referencedColumnName: 'id', foreignKeyConstraintName: 'tests_created_by_fk' })
  creator: AdminEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'updated_by', referencedColumnName: 'id', foreignKeyConstraintName: 'tests_updated_by_fk' })
  updater: AdminEntity;

  @OneToOne(() => AdminEntity)
  @JoinColumn({ name: 'deleted_by', referencedColumnName: 'id', foreignKeyConstraintName: 'tests_deleted_by_fk' })
  eraser: AdminEntity;
}
