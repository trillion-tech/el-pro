
// import { UserModel } from '../entity/user.entity';

// const Model = {
//   user: UserModel
// }

const unique = async (value, options: string): Promise<boolean> => {
  // let modelName = options.split(':')[0];
  // let colName = options.split(':')[1]; 
  // let conditions = {};
  // conditions[colName] = value;
  
  // let model = Model[modelName];
  // if(!model) throw new Error("Model does not exists");

  // const result = await model.find(conditions).exec();
  
  // if(result.length !== 0) {
  //   throw new Error("is exsist");
  // }

  // return true;
  return true;
}

const required = (value) => {
  if(!value) {
    throw new Error('is required');
  }
  return true;
}

const min = (value, length: number) => {
  if(!value || value.length < length) {
    throw new Error(`have min ${length}`);
  } 
  return true;
}

const min_value = (value: number, min_value: number) => {
  if(!value || value < min_value) {
    throw new Error(`have min value ${min_value}`);
  } 
  return true;
}

export default {
  unique,
  required,
  min,
  min_value
}