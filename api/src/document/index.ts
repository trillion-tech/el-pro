import { OpenApi } from 'ts-openapi';
import { configPaths } from './paths';

export function initDocument(openApi: OpenApi) {
  for (const config of configPaths) {
    openApi.addPath(config.path, config.config, config.visible);
  }
}
