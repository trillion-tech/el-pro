import { PathInput } from 'ts-openapi/lib/openapi/openapi.types';
import ApisV1Config from '../../modules/v1/document/paths';
import AdminConfig from '../../modules/admin/document/paths';

export type PathConfigOpenApi = {
  path: string;
  config: PathInput;
  visible: boolean;
}

export const configPaths: Array<PathConfigOpenApi> = [...ApisV1Config, ...AdminConfig];