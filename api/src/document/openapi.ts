import { bearerAuth, OpenApi } from 'ts-openapi';
import swaggerUi from 'swagger-ui-express';

// create an OpenApi instance to store definitions
export const openApiInstance = new OpenApi(
  'latest', // API version
  'EL-PRO-API', // API title
  'Describing how to keep APIs documented.', // API description
  'trillion.developer@gmail.com', // API maintainer
);

// declare servers for the API
openApiInstance.setServers([
  {
    url: 'http://localhost:3002/api',
    description: 'API Router',
  },
]);

// // set API license
openApiInstance.setLicense(
  'Apache License, Version 2.0', // API license name
  'http://www.apache.org/licenses/LICENSE-2.0', // API license url
  'http://localhost:3002/terms/', // API terms of service
);

openApiInstance.declareSecurityScheme('Authorization', bearerAuth());
openApiInstance.declareSecurityScheme('AuthorizationAdmin', bearerAuth());

export function initOpenApi(app, openApi: OpenApi) {
  // generate our OpenApi schema
  const openApiJson = openApi.generateJson();

  // we'll create an endpoint to reply with openapi schema
  app.get('/openapi.json', function (_req, res) {
    res.json(openApiJson);
  });
  // this will make openapi UI available with our definition
  app.use('/document', swaggerUi.serve, swaggerUi.setup(openApiJson));
}
