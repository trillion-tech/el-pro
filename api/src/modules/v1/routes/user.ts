import Router from 'express-promise-router';
import { Request, Response } from 'express';
import AuthController from '../controllers/auth/auth.controller';
import UserController from '../controllers/user.controller';
import LessonController from '../controllers/lesson.controller';
import Passport from 'passport';

const route = Router();

const getControllerInstance = async (req: Request, res: Response, next: any, controller, method: string) => {
  // validationResult(req);
  const controllerInstance = new controller();
  return controllerInstance[method](req, res, next);
};

// User Auth
route
  .post(
    '/signUp',
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, AuthController, 'signUp'),
  )
  .post('/signIn', async (req: Request, res: Response, next: any) =>
    getControllerInstance(req, res, next, AuthController, 'signIn'),
  )
  .get(
    '/profile',
    Passport.authenticate('api-user', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, UserController, 'profile'),
  )
  .post(
    '/profile',
    Passport.authenticate('api-user', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, UserController, 'update'),
  )
  .post(
    '/change-password',
    Passport.authenticate('api-user', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, UserController, 'changePassword'),
  );

// lesson
route
  .get(
    '/lesson/list',
    Passport.authenticate('api-user', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, LessonController, 'list'),
  )
  .get(
    '/lesson/detail/:lessonId',
    Passport.authenticate('api-user', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, LessonController, 'detail'),
  );

export default route;
