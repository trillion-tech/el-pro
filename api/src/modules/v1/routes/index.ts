import Router from 'express-promise-router';
import UserRouters from './user';

const routes = Router();

// User
routes.use('/user', UserRouters);

// Route.use('/api/admin', AdmintRoutes);
// Test connect server
routes.get('/', (req, res, next) => {
  return res.status(200).json({
    message: 'Server is OK!',
    version: 'v1'
  });
});

export default routes;
