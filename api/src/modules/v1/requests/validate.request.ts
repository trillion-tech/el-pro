class Validate {
  public errors: Array<any>;

  public request: any;
  public response: any;

  public next: any;

  async validate() {
    // sequential processing, stops running validations chain if one fails.
    for (const validation of this.rules()) {
      const result = await validation.run(this.request);
      this.errors = result.array();
      if (!result.isEmpty()) {
        return this.response.status(422).json({ errors: this.errors });
      }
    }

    return false;
  }

  rules(): Array<any> {
    return [];
  }
}

export default Validate;
