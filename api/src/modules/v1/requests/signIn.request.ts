import { body } from 'express-validator';
import rules from '../../../request/rules';
import Validate from './validate.request';

class PostSigInUpRequest extends Validate {
  
  constructor(request, response, next) {
    super();
    this.request = request;
    this.response = response;
    this.next = next;
  }

  rules(): Array<any> {
    return [
      body('username').notEmpty(),
      body('username').custom((value) => rules.min(value, 1)),
      body('username').isAlphanumeric(),
      body('password').notEmpty(),
      body('password').custom((value) => rules.min(value, 1)),
    ];
  }
}

export default PostSigInUpRequest;
