import Controller from '../../../controllers/base.controller';
import { Request, Response } from 'express';
// service
import UserService from '../services/user.service';

//utils
import Logger from '../../../utils/logger.util';

class UserController extends Controller {
  private userService: UserService;
  private readonly logger: Logger;

  constructor() {
    super();
    this.userService = new UserService();
    this.logger = new Logger();
    this.logger.info('AuthController is initialized');
  }

  /**
   * Get user profile
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns UserEntity | null
   */
  async profile(req: Request, res: Response, next: any) {
    try {
      const auth = this.auth(req);
      const user = await this.userService.findById(auth.id);

      if (!user) {
        return this.json(res, 400, {
          error: 'User is not valid',
        });
      }

      return this.json(res, 200, user);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  /**
   * Update user profile
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns UserEntity | null
   */
  async update(req: Request, res: Response, next: any) {
    try {
      const auth = this.auth(req);
      const user = await this.userService.updateProfile(auth.id, {
        username: req.body?.username
      });

      if (!user) {
        return this.json(res, 400, {
          error: 'User is not valid',
        });
      }

      return this.json(res, 200, user);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  /**
   * Update user profile
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns UserEntity | null
   */
  async changePassword(req: Request, res: Response, next: any) {
    try {
      const auth = this.auth(req);
      const user = await this.userService.changePassword(auth.id, {
        oldPassword: req.body?.oldPassword,
        newPassword: req.body?.newPassword,
      });

      return this.json(res, 200, user);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  private auth(req: Request | any) {
    return req.user || null;
  }
}

export default UserController;