import { Lesson as LessonEntity } from '../../../entity/lesson.entity';

class LessonService {
  async paginate(page: number, limit: number): Promise<any> {
    return await LessonEntity.paginate(page, limit);
  }

  async findById(id: number): Promise<LessonEntity | null> {
    return await LessonEntity.findOneBy({
      id
    });
  }
}

export default LessonService;
