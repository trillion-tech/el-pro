import { User as UserEntity } from '../../../entity/user.entity';
import { hash } from '../../../helpers';
import { UpdateUserProfileOptions, UpdateUserPasswordOptions } from '../types/user';

class UserService {
  async createNewUser(username: string, password: string): Promise<UserEntity> {
    const user = await UserEntity.findOne({
      where: {
        username,
      },
    });

    if (user) {
      throw new Error('User name has exists');
    }

    const newUser = new UserEntity();
    newUser.username = username;
    newUser.password = await hash(password);

    return await newUser.save();
  }

  async findByUserName(username: string): Promise<UserEntity | null> {
    return await UserEntity.findOneBy({
      username,
    });
  }

  async verifyAuth(username: string, password: string): Promise<UserEntity | null> {
    const passwordHash = await hash(password);

    return await UserEntity.findOneBy({
      username,
      password: passwordHash,
    });
  }

  async findById(id: number): Promise<UserEntity | null> {
    return await UserEntity.findOneBy({
      id,
    });
  }

  async updateProfile(id: number, profile: UpdateUserProfileOptions): Promise<UserEntity> {
    const user = await UserEntity.findOneBy({
      id,
    });

    if (!user) {
      throw new Error('User is not found');
    }

    return await user.update({
      data: profile,
    });
  }

  async changePassword(id: number, data: UpdateUserPasswordOptions): Promise<UserEntity> {
    const oldPasswordHash = await hash(data.oldPassword);

    const user = await UserEntity.findOneBy({
      id,
      password: oldPasswordHash,
    });

    if (!user) {
      throw new Error('User is not found');
    }

    const newPasswordHash = await hash(data.newPassword);

    return await user.update({
      data: {
        password: newPasswordHash,
      },
    });
  }
}

export default UserService;
