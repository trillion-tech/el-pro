import { textPlain, Types } from 'ts-openapi';
import { PathConfigOpenApi } from '../../../../document/paths';

const signUpConfig: PathConfigOpenApi = {
  path: '/v1/user/signUp', // this is API path
  config: {
    // API method
    post: {
      description: 'Register new user', // Method description
      summary: 'Register new user APIs', // Method summary
      operationId: 'signUp-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        body: Types.Object({
          description: 'Customer data to create',
          properties: {
            username: Types.String({
              description: 'User Name',
              maxLength: 100,
              required: true,
              default: 'user_test_1',
              example: 'user_test_1',
            }),
            password: Types.String({
              description: 'Password',
              maxLength: 100,
              required: true,
              default: 'password_test',
              example: 'password_test',
            }),
          },
        }),
        params: {
          // test: '1',
        },
      },
      tags: ['Sign Up new user'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const signInConfig: PathConfigOpenApi = {
  path: '/v1/user/signIn', // this is API path
  config: {
    // API method
    post: {
      description: 'User login', // Method description
      summary: 'User login APIs', // Method summary
      operationId: 'signIn-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        body: Types.Object({
          description: 'Customer data to create',
          properties: {
            username: Types.String({
              description: 'User Name',
              maxLength: 100,
              required: true,
            }),
            password: Types.String({
              description: 'Password',
              maxLength: 100,
              required: true,
            }),
          },
        })
      },
      tags: ['Sign in user'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const userGetProfile: PathConfigOpenApi = {
  path: '/v1/user/profile', // this is API path
  config: {
    // API method
    get: {
      description: 'User Profile', // Method description
      summary: 'User Profile APIs', // Method summary
      operationId: 'userProfile-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      security: [
        {
          Authorization: [],
        }
      ],
      tags: ['Get User Profile'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const userUpdateProfile: PathConfigOpenApi = {
  path: '/v1/user/profile', // this is API path
  config: {
    // API method
    post: {
      description: 'User Update Profile', // Method description
      summary: 'User Update Profile APIs', // Method summary
      operationId: 'userUpdateProfile-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        body: Types.Object({
          description: 'Customer data to create',
          properties: {
            username: Types.String({
              description: 'User Name',
              maxLength: 100,
              required: true,
            }),
          },
        })
      },
      security: [
        {
          Authorization: [],
        }
      ],
      tags: ['Update User Profile'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const userChangePassword: PathConfigOpenApi = {
  path: '/v1/user/change-password', // this is API path
  config: {
    // API method
    post: {
      description: 'User change password', // Method description
      summary: 'User change password APIs', // Method summary
      operationId: 'userChangePassword-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        body: Types.Object({
          description: 'Customer data to create',
          properties: {
            oldPassword: Types.String({
              description: 'Old Password',
              maxLength: 100,
              required: true,
            }),
            newPassword: Types.String({
              description: 'New Password',
              maxLength: 100,
              required: true,
            }),
          },
        })
      },
      security: [
        {
          Authorization: [],
        }
      ],
      tags: ['User change password'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

export default [signUpConfig, signInConfig, userGetProfile, userUpdateProfile, userChangePassword];
