import UserConfig from './user';
import LessonConfig from './lesson';

export default [
  ...UserConfig,
  ...LessonConfig
];