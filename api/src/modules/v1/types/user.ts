export type UpdateUserPasswordOptions = {
  oldPassword: string;
  newPassword: string
};

export type UpdateUserProfileOptions = {
  username: string
};