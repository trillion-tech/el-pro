import Router from 'express-promise-router';
import { Request, Response } from 'express';
import AuthController from '../controllers/auth/auth.controller';
import AdminController from '../controllers/admin.controller';
import LessonController from '../controllers/lesson.controller';
import Passport from 'passport';

const route = Router();

const getControllerInstance = async (req: Request, res: Response, next: any, controller, method: string) => {
  // validationResult(req);
  const controllerInstance = new controller();
  return controllerInstance[method](req, res, next);
};

// User Auth
route
  .post(
    '/signUp',
    Passport.authenticate('api-admin', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, AuthController, 'signUp'),
  )
  .post('/signIn', async (req: Request, res: Response, next: any) =>
    getControllerInstance(req, res, next, AuthController, 'signIn'),
  )
  .get(
    '/profile',
    Passport.authenticate('api-admin', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, AdminController, 'profile'),
  );

// admin
route.get(
  '/list',
  Passport.authenticate('api-admin', { session: false }),
  async (req: Request, res: Response, next: any) =>
    await getControllerInstance(req, res, next, AdminController, 'list'),
);

route.get(
  '/detail/:adminId',
  Passport.authenticate('api-admin', { session: false }),
  async (req: Request, res: Response, next: any) =>
    await getControllerInstance(req, res, next, AdminController, 'detail'),
);


// lesson
route
  .get(
    '/lesson/list',
    Passport.authenticate('api-admin', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, LessonController, 'list'),
  )
  .get(
    '/lesson/detail/:lessonId',
    Passport.authenticate('api-admin', { session: false }),
    async (req: Request, res: Response, next: any) =>
      await getControllerInstance(req, res, next, LessonController, 'detail'),
  );

export default route;
