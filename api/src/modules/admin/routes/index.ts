import Router from 'express-promise-router';
import AdminRouters from './admin';

const routes = Router();

// Admin
routes.use('/admin', AdminRouters);

// Route.use('/api/admin', AdmintRoutes);
// Test connect server
routes.get('/', (req, res, next) => {
  return res.status(200).json({
    message: 'Server is OK!',
    version: 'v1'
  });
});

export default routes;
