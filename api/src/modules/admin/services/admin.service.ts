import { Admin as AdminEntity } from '../../../entity/admin.entity';
import { hash } from '../../../helpers';

class AdminService {
  async paginate(page: number, limit: number): Promise<any> {
    return await AdminEntity.paginate(page, limit);
  }

  async createNewUser(username: string, password: string): Promise<AdminEntity> {
    const user = await AdminEntity.findOne({
      where: {
        username,
      },
    });

    if (user) {
      throw new Error('User name has exists');
    }

    const newUser = new AdminEntity();
    newUser.username = username;
    newUser.password = await hash(password);

    return await newUser.save();
  }

  async findByUserName(username: string): Promise<AdminEntity | null> {
    return await AdminEntity.findOneBy({
      username,
    });
  }

  async verifyAuth(username: string, password: string): Promise<AdminEntity | null> {
    return await AdminEntity.findOneBy({
      username,
      password,
    });
  }

  async findById(id: number): Promise<AdminEntity | null> {
    return await AdminEntity.findOneBy({
      id,
    });
  }
}

export default AdminService;
