import { User as UserEntity } from '../../../entity/user.entity';

class UserService {
  async findByUserName(username: string): Promise<UserEntity | null> {
    return await UserEntity.findOneBy({
      username,
    });
  }

  async findById(id: number): Promise<UserEntity | null> {
    return await UserEntity.findOneBy({
      id,
    });
  }
}

export default UserService;
