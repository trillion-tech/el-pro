import { textPlain, Types } from 'ts-openapi';
import { PathConfigOpenApi } from '../../../../document/paths';

const lessonListConfig: PathConfigOpenApi = {
  path: '/admin/lesson/list', // this is API path
  config: {
    // API method
    get: {
      description: 'Get List Lesson', // Method description
      summary: 'Get List Lesson APIs', // Method summary
      operationId: 'adminGetLessonList-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        query: {
          page: Types.Integer({
            description: 'Current Page',
            required: true,
            default: 1
          }),
          limit: Types.Integer({
            description: 'Current Page',
            required: true,
            default: 15
          }),
        },
      },
      security: [
        {
          AuthorizationAdmin: [],
        }
      ],
      tags: ['Get lesson list'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const lessonDetailConfig: PathConfigOpenApi = {
  path: '/admin/lesson/detail/:lessonId', // this is API path
  config: {
    // API method
    get: {
      description: 'Get Detail Lesson', // Method description
      summary: 'Get Detail Lesson APIs', // Method summary
      operationId: 'adminGetLessonDetail-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        params: {
          lessonId: Types.Integer({
            description: 'Lesson id',
            required: true,
            default: 1
          }),
        },
      },
      security: [
        {
          AuthorizationAdmin: [],
        }
      ],
      tags: ['Get lesson Detail'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};


export default [lessonListConfig, lessonDetailConfig];
