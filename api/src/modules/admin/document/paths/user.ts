import { textPlain, Types } from 'ts-openapi';
import { PathConfigOpenApi } from '../../../../document/paths';

const signUpConfig: PathConfigOpenApi = {
  path: '/admin/signUp', // this is API path
  config: {
    // API method
    post: {
      description: 'Register new user', // Method description
      summary: 'Register new user APIs', // Method summary
      operationId: 'adminSignUp-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        body: Types.Object({
          description: 'Customer data to create',
          properties: {
            username: Types.String({
              description: 'User Name',
              maxLength: 100,
              required: true,
              default: 'user_test_1',
              example: 'user_test_1',
            }),
            password: Types.String({
              description: 'Password',
              maxLength: 100,
              required: true,
              default: 'password_test',
              example: 'password_test',
            }),
          },
        }),
        params: {
          // test: '1',
        },
      },
      tags: ['Sign Up new user'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const signInConfig: PathConfigOpenApi = {
  path: '/admin/signIn', // this is API path
  config: {
    // API method
    post: {
      description: 'User login', // Method description
      summary: 'User login APIs', // Method summary
      operationId: 'adminSignIn-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        body: Types.Object({
          description: 'Customer data to create',
          properties: {
            username: Types.String({
              description: 'User Name',
              maxLength: 100,
              required: true,
            }),
            password: Types.String({
              description: 'Password',
              maxLength: 100,
              required: true,
            }),
          },
        }),
        params: {
          // test: '1',
        },
      },
      tags: ['Sign in admin'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const adminProfile: PathConfigOpenApi = {
  path: '/admin/profile', // this is API path
  config: {
    // API method
    get: {
      description: 'Admin Profile', // Method description
      summary: 'User Profile APIs', // Method summary
      operationId: 'adminProfile-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      security: [
        {
          AuthorizationAdmin: [],
        }
      ],
      tags: ['Get User Profile'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

export default [signUpConfig, signInConfig, adminProfile];
