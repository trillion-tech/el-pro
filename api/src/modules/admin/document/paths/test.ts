import { textPlain, Types } from 'ts-openapi';
import { PathConfigOpenApi } from '../../../../document/paths';

const testListConfig: PathConfigOpenApi = {
  path: '/admin/test/list', // this is API path
  config: {
    // API method
    get: {
      description: 'Get List test', // Method description
      summary: 'Get List test APIs', // Method summary
      operationId: 'adminGetTestList-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        query: {
          page: Types.Integer({
            description: 'Current Page',
            required: true,
            default: 1
          }),
          limit: Types.Integer({
            description: 'Current Page',
            required: true,
            default: 15
          }),
        },
      },
      security: [
        {
          AuthorizationAdmin: [],
        }
      ],
      tags: ['Get test list'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};

const testDetailConfig: PathConfigOpenApi = {
  path: '/admin/test/detail/:testId', // this is API path
  config: {
    // API method
    get: {
      description: 'Get Detail test', // Method description
      summary: 'Get Detail test APIs', // Method summary
      operationId: 'adminGetTestDetail-v1', // an unique operation id
      responses: {
        // here we declare the response types
        200: textPlain('Successful Operation'),
      },
      requestSchema: {
        params: {
          testId: Types.Integer({
            description: 'test id',
            required: true,
            default: 1
          }),
        },
      },
      security: [
        {
          AuthorizationAdmin: [],
        }
      ],
      tags: ['Get test Detail'], // these tags group your methods in UI
    },
  },
  visible: true, // make method visible
};


export default [testListConfig, testDetailConfig];
