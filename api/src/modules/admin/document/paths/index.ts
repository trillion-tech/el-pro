import UserConfig from './user';
import LessonConfig from './lesson';
import TestConfig from './test';

export default [
  ...UserConfig,
  ...LessonConfig,
  ...TestConfig
];