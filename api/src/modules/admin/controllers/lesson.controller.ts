import Controller from '../../../controllers/base.controller';
import { Request, Response } from 'express';
// service
import LessonService from '../services/lesson.service';

//utils
import Logger from '../../../utils/logger.util';

class LessonController extends Controller {
  private lessonService: LessonService;
  private readonly logger: Logger;

  constructor() {
    super();
    this.lessonService = new LessonService();
    this.logger = new Logger();
    this.logger.info('AuthController is initialized');
  }

  /**
   * Get lesson list
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns UserEntity | null
   */
  async list(req: Request, res: Response, next: any) {
    try {
      const query: any = req?.query || {};
      const data = await this.lessonService.paginate(Number(query?.page || 1), Number(query?.limit || 15));
      return this.json(res, 200, data);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  /**
   * Get lesson list
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns UserEntity | null
   */
  async detail(req: Request, res: Response, next: any) {
    try {
      if (!req.params['lessonId']) {
        return this.json(res, 404, {}); 
      }

      const lesson = await this.lessonService.findById(Number(req.params['lessonId']));

      if (!lesson) {
        return this.json(res, 404, {}); 
      }

      return this.json(res, 200, lesson);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

    /**
   * Get lesson list
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns UserEntity | null
   */
    async store(req: Request, res: Response, next: any) {
      try {
        if (!req.params['lessonId']) {
          return this.json(res, 404, {}); 
        }
  
        const lesson = await this.lessonService.findById(Number(req.params['lessonId']));
  
        if (!lesson) {
          return this.json(res, 404, {}); 
        }
  
        return this.json(res, 200, lesson);
      } catch (error) {
        console.log(error);
        return this.json(res, 500, {
          error: 'System Error !',
        });
      }
    }

  private auth(req: Request | any) {
    return req.user || null;
  }
}

export default LessonController;
