import { Request, Response } from 'express';
import * as JWT from 'jsonwebtoken';

import { hash as HashConfig, jwt as JWTConfig } from '../../../../config';
import Controller from '../../../../controllers/base.controller';
import { hash } from '../../../../helpers';

// service
import AdminService from '../../services/admin.service';

//utils

import Logger from '../../../../utils/logger.util';

// request
import PostSignUpRequest from '../../requests/signUp.request';
import PostSigInUpRequest from '../../requests/signIn.request';

class AuthController extends Controller {
  private readonly adminService: AdminService;
  private readonly logger: Logger;

  constructor() {
    super();
    this.adminService = new AdminService();
    this.logger = new Logger();
    this.logger.info('AuthController is initialized');
  }

  async signUp(req: Request, res: Response, next: any) {
    try {
      const validation = new PostSignUpRequest(req, res, next);
      const valid = await validation.validate();

      if (valid) {
        return valid;
      }

      const body = req.body;

      const newUser = await this.adminService.createNewUser(body['username'], body['password']);

      return this.json(res, 200, this.removeProp(newUser, ['password']));
    } catch (err) {
      console.log(err);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  async signIn(req: Request, res: Response, next: any) {
    try {
      const validation = new PostSigInUpRequest(req, res, next);
      const valid = await validation.validate();

      if (valid) {
        return valid;
      }

      const body = req.body;

      this.logger.info('AuthController signUp route', body);

      const password = await hash(req.body.password);

      const user = await this.adminService.verifyAuth(body['username'], password);

      if (!user) {
        return this.json(res, 400, {
          error: 'username or password is not correct',
        });
      }

      let token = this.generateJwtToken(user.id, user.username);

      return this.json(res, 200, {
        token: token,
        user: this.removeProp(user, ['password']),
      });
    } catch (err) {
      console.log(err);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  private generateJwtToken(userId: number, username: string) {
    return JWT.sign(
      {
        iss: HashConfig.salt,
        sub: {
          id: userId,
          username: username
        },
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + JWTConfig.expiredDays),
      },
      JWTConfig.secret,
    );
  }
}
export default AuthController;
