import Controller from '../../../controllers/base.controller';
import { Request, Response } from 'express';
// service
import AdminService from '../services/admin.service';

//utils
import Logger from '../../../utils/logger.util';

class AdminController extends Controller {
  private adminService: AdminService;
  private readonly logger: Logger;

  constructor() {
    super();
    this.adminService = new AdminService();
    this.logger = new Logger();
    this.logger.info('AuthController is initialized');
  }

  /**
   * Get admin profile
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns AdminEntity | null
   */
  async detail(req: Request, res: Response, next: any) {
    try {
      if (!req.params['adminId']) {
        return this.json(res, 404, {}); 
      }

      const admin = await this.adminService.findById(Number(req.params['adminId']));

      if (!admin) {
        return this.json(res, 404, {}); 
      }

      return this.json(res, 200, admin);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  /**
   * Get admin list
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns AdminEntity | null
   */
  async list(req: Request, res: Response, next: any) {
    try {
      const query: any = req?.query || {};
      const data = await this.adminService.paginate(Number(query?.page || 1), Number(query?.limit || 15));
      return this.json(res, 200, data);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

    /**
   * Get admin profile
   * @param req Request
   * @param res Response
   * @param next  any
   * @returns AdminEntity | null
   */
  async profile(req: Request, res: Response, next: any) {
    try {
      const auth = this.auth(req);
      const user = await this.adminService.findById(auth.id);

      if (!user) {
        return this.json(res, 400, {
          error: 'User is not valid',
        });
      }

      return this.json(res, 200, user);
    } catch (error) {
      console.log(error);
      return this.json(res, 500, {
        error: 'System Error !',
      });
    }
  }

  private auth(req: Request | any) {
    return req.user || null;
  }
}

export default AdminController;
