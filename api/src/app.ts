import express, { Express, Request, Response } from 'express';
import routes from './routes';
import * as bodyParser from 'body-parser';
import logger from 'morgan';
import PassportMiddleware from './middlewares/passport.middleware';
import cors from 'cors';
import connection from './entity/db';

// Connect database

const app = connection().then(() => {
  const App: Express = express();

  // Config passport
  new PassportMiddleware();

  App.use('/assets', express.static(__dirname + '/assets'));

  App.use(bodyParser.json());
  App.use(cors());
  App.use(logger('dev'));
  App.use(routes);
  // // Catch 404 Errors and forward them to error handler
  App.use((req, res, next) => {
    const err = new Error('Not Found');
    next(err);
  });

  // Error handler function
  App.use((err: any, req: Request, res: Response, next: any) => {
    const error = App.get('env') === 'development' ? err : {};
    const status = err.status || 500;

    // response to client
    return res.status(status).json({
      error: {
        message: error.message,
      },
    });
  });

  return App;
}).catch((error) => {
  throw error;
  
});

export default app;
