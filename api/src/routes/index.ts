import Router from 'express-promise-router';
import ApiRouter from './api';

const routes = Router();
// User



routes.use('/api', ApiRouter);

routes.get('/', (req, res, next) => {
  return res.status(200).json({
    message: 'Server is OK!',
  });
});

routes.get('/api/app-info', (req, res, next) => {
  return res.status(200).json({
    version: '1.0.0',
  });
});

export default routes;
