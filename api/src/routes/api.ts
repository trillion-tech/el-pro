import Router from 'express-promise-router';
import ApiV1 from '../modules/v1/routes';
import { openApiInstance, initOpenApi } from '../document/openapi';
import { initDocument } from '../document';

const routes = Router();

// declare our hello world api
initDocument(openApiInstance);

// initializes schema endpoint and UI
initOpenApi(routes, openApiInstance);
// User
routes.use('/v1', ApiV1);

export default routes;
