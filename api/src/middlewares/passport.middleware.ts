import passport from 'passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { jwt } from '../config';
import { User as UserEntity } from '../entity/user.entity';

class PassportMiddleware {
  private jwtData: Object;

  constructor() {
    this.jwtData = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken('Authorization'),
      secretOrKey: jwt.secret,
    };
    this.handle();
  }

  public handle(): void {
    this.jwtPassport();
  }

  protected jwtPassport(): void {
    passport.use(
      'api-user',
      new Strategy(this.jwtData, (payload, done) => this.authenticate(payload, done, 'api-user')),
    );

    passport.use(
      'api-admin',
      new Strategy(this.jwtData, (payload, done) => this.authenticate(payload, done, 'api-admin')),
    );
  }

  public async authenticate(payload, done, guard: string) {
    try {
      let auth: any = null;

      if (guard == 'api-admin') auth = {};
      else
        auth = await UserEntity.findOne({
          where: {
            username: payload.sub.username,
            id: payload.sub.id,
          },
        });

      if (!auth) return done(null, false);

      done(null, auth);
    } catch (error) {
      done(error, false);
    }
  }
}

export default PassportMiddleware;
