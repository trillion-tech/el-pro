export default class BaseController {
  json(res, status, data) {
    return res.status(status).json({
      data: data, 
    });
  }

  removeProp(objectData: Object, exceptKeys: Array<string>) {
    exceptKeys.forEach(key => {
      delete objectData[key];
    })
    return objectData;
  }
}