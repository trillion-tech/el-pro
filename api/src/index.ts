import App from './app';
import { app } from './config';

App.then((appInstance) => {
  appInstance.listen(app.port, () => console.log(`Server is listening on port ${app.port}`))
});