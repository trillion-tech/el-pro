import { env } from '../helpers';

export const app = {
  port: env('APP_PORT')
}