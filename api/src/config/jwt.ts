import { env } from '../helpers';

export const jwt = {
  secret:  env('JWT_SECRET'),
  expiredDays: env('JWT_EXPIRED_DAYS', 3)
}