import { env } from '../helpers';
import { MysqlConnectionOptions } from 'typeorm/browser/driver/mysql/MysqlConnectionOptions';

export const database = {
  default: env('DATABASE_TYPE', 'mysql'),
  mysql: {
    type: 'mysql',
    host: env('DATABASE_HOST', '127.0.0.1'),
    port: env('DATABASE_PORT', 3306),
    database: env('DATABASE_NAME', 'el_pro_db'),
    username: env('DATABASE_USERNAME', 'root'),
    password: env('DATABASE_PASSWORD', 'root'),
    // ssl: !!env('DATABASE_SSL', 'false'),
    maxQueryExecutionTime: env('DATABASE_MAX_EXECUTE_TIME', 30),
    logging: !!env('DATABASE_LOGGING', 'true'),
    // driver: 'mysql2',
    debug: !env('DATABASE_DEBUG', 'true'),
    // connectorPackage: 'mysql2'
  } as MysqlConnectionOptions,
};
