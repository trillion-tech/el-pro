import { env } from '../helpers';

export const filesystems = {
  driver: env('FILESYSTEM_DRIVER')
}