import { env } from '../helpers';

export const mail = {
  host: env('MAIL_HOST'),
  port: env('MAIL_PORT'),
  username: env('MAIL_USERNAME'),
  password: env('MAIL_PASSWORD'),
  driver: env('MAIL_DRIVER'),
  encryption: env('MAIL_ENCRYPTION')
}