import { env } from '../helpers';

export const hash = {
  salt: env('SALT')
}