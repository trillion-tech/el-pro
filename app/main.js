const { app, BrowserWindow } = require('electron/main');
const path = require('node:path');

const isDev = true;

function createWindow() {
  const win = new BrowserWindow({
    width: 1200,
    height: 650,
    minWidth: 1200,
    minHeight: 650,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
    },
    title: 'EL PRO',
    // frame: false,
    // transparent: true,
  });

  win.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
  win.webContents.openDevTools();
}

app.whenReady().then(() => {
  createWindow();

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
