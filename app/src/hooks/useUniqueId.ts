import * as React from 'react';
import { useEffect, useState, useLayoutEffect as reactUseLayoutEffect } from 'react';
const useLayoutEffect = typeof window !== 'undefined' ? reactUseLayoutEffect : useEffect;

let serverHandoffComplete = false;
let id = 0;
function genId() {
  return ++id;
}
function useId(providedId?: number | string | undefined | null) {
  if (typeof React.useId === 'function') {
    // @ts-expect-error
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const id = React.useId(providedId);
    return providedId != null ? providedId : id;
  }
  const initialId = providedId ?? (serverHandoffComplete ? genId() : null);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [id, setId] = useState(initialId);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  useLayoutEffect(() => {
    if (id === null) {
      setId(genId());
    }
  }, [id]);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    if (!serverHandoffComplete) {
      serverHandoffComplete = true;
    }
  }, []);

  return providedId ?? id ?? undefined;
}
const useUniqueId = (prefix?: string): string => {
  return (prefix ? prefix + '-' : '') + useId();
};

export default useUniqueId;
