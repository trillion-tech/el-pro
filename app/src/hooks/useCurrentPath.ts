export const useCurrentPath = () => {
  return window.location?.pathname;
};
