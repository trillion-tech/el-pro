import React, { createContext, useEffect, useReducer } from 'react';
import { axiosDefault } from 'src/util/axios';
import { POST_SIGN_IN_URL_API, GET_PROFILE_URL_API, POST_SIGN_UP_URL_API } from 'src/util/constants/api-url';
// import jwtDecode from 'jwt-decode';
import { API_STATUS_SUCCESS } from 'src/util/axios/api-status-code';
import { ApiResponse, responseError, responseObject } from 'src/interfaces/api';
// import { LoadingPage } from 'src/components/atoms/loading-page';
import { IUserInfo } from 'src/interfaces/user';
import { IFormUserSignUp, IFormUserSignIn } from 'src/interfaces/form';

const TOKEN_KEY = 'app_dev';

const ACTION_TYPES = {
  LOGOUT: 'LOGOUT',
  SIGN_IN: 'SIGN_IN',
  INIT: 'INIT',
  SIGN_UP: 'SIGN_UP',
};

interface IInitialState {
  isAuthenticated: boolean;
  isInitialised: boolean;
  isRegister: boolean;
  user: IUserInfo | null;
}

const initialState: IInitialState = {
  isAuthenticated: false,
  isInitialised: false,
  isRegister: false,
  user: null,
};

const setSession = (accessToken: string | null) => {
  if (accessToken) {
    localStorage.setItem(TOKEN_KEY, accessToken);
  } else {
    localStorage.removeItem(TOKEN_KEY);
  }
};

export const getSession = (): string | null => {
  return localStorage.getItem(TOKEN_KEY) || null;
};

const AuthContext = createContext({
  ...initialState,
  signIn: (_data: IFormUserSignIn): Promise<ApiResponse<any>> => Promise.resolve({ success: false, message: 'error' }),
  logout: () => {},
  signUp: (_data: IFormUserSignUp): Promise<ApiResponse<any>> => Promise.resolve({ success: false, message: 'error' }),
  getProfile: (): Promise<ApiResponse<any>> => Promise.resolve({ success: false, message: 'error' }),
});

interface IAction {
  type: string;
  payload?: any;
}

const reducer = (state: IInitialState, action: IAction) => {
  console.log('reducer', state, action);
  switch (action.type) {
    case ACTION_TYPES.INIT:
      // eslint-disable-next-line no-case-declarations
      const user = action?.payload?.user;

      return {
        isAuthenticated: true,
        isInitialised: true,
        isRegister: false,
        user,
      };

    case ACTION_TYPES.SIGN_IN: {
      const { user } = action.payload;
      return {
        isAuthenticated: true,
        isInitialised: true,
        isRegister: false,
        user,
      };
    }

    case ACTION_TYPES.LOGOUT: {
      return {
        isInitialised: false,
        isAuthenticated: false,
        isRegister: true,
        user: null,
      };
    }

    case ACTION_TYPES.SIGN_UP: {
      const { user } = action.payload;
      return {
        isAuthenticated: false,
        isInitialised: true,
        isRegister: true,
        user,
      };
    }

    default:
      return { ...state };
  }
};

export const AuthProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const signIn = async (data: IFormUserSignIn) => {
    // TODO: update api
    try {
      // const response = await axiosDefault.post(POST_SIGN_IN_URL_API, {
      //   ...data,
      // });

      console.log('signIn', data);
      const response = {
        data: {
          data: {
            user: {
              username: data.username,
              id: 1,
            },
            access_token: 'TEST_TOKEN',
          },
        },
        status: API_STATUS_SUCCESS,
        success: true,
        statusText: 'success',
        headers: {},
        config: {},
      };

      if (response.status === API_STATUS_SUCCESS) {
        const token = response.data.data.access_token;
        setSession(token);
        dispatch({
          type: 'SIGN_IN',
          payload: {
            user: response.data.data.user,
          },
        });
        return responseObject(response);
      }

      return responseError({ status: response.status, message: 'login failed' });
    } catch (err) {
      return responseError(err);
    }
  };

  const signUp = async (data: IFormUserSignUp) => {
    try {
      // TODO: update api
      // const response = await axiosDefault.post(POST_SIGN_UP_URL_API, {
      //   ...data,
      // });

      console.log('signUp', data);

      const response = {
        data: {
          data: {
            user: {
              username: data.username,
              id: 1,
            },
            access_token: 'TEST_TOKEN',
          },
        },
        status: API_STATUS_SUCCESS,
        success: true,
        statusText: 'success',
        headers: {},
        config: {},
      };

      if (response.status === API_STATUS_SUCCESS) {
        const token = response.data.data.access_token;
        setSession(token);
        dispatch({
          type: 'SIGN_IN',
          payload: {
            user: response.data.data.user,
          },
        });
        return responseObject(response);
      }
      return responseError({ status: response.status, message: 'register failed' });
    } catch (err) {
      return responseError(err);
    }
  };

  const logout = () => {
    setSession(null);
    dispatch({
      type: 'LOGOUT',
      payload: {
        user: null,
      },
    });
  };

  const getProfile = async () => {
    try {
      const response = await axiosDefault.get(GET_PROFILE_URL_API);
      if (response.status === API_STATUS_SUCCESS) {
        updateUserInfo(response.data.data);
        return responseObject(response);
      }
      return responseError({ status: response.status, message: 'get profile error' });
    } catch (e) {
      logout();
      return responseError(e);
    }
  };

  const updateUserInfo = (user: any) => {
    dispatch({
      type: 'INIT',
      payload: {
        user,
      },
    });
  };

  const init = async () => {
    // const path = window.location.href;
    // const decodedPath = decodeURIComponent(path);
    // const cid = getMemberIdFromPath(decodedPath);
    // const accessToken = getTokenFormPath(decodedPath);
    // const userTestId = getUserTestFromPath(decodedPath) ?? '';
    // if (accessToken && cid) {
    //   setSession(null);
    //   await login(cid, accessToken, userTestId);
    // } else {
    //   const response = await getProfile();
    //   if (response.success) {
    //     dispatch({
    //       type: 'INIT',
    //       payload: {
    //         user: response.data,
    //       },
    //     });
    //   }
    // }
  };

  useEffect(() => {
    init();
  }, []);
  // if ((!state.isInitialised || !state.isAuthenticated) && !state.isRegister) {
  //   return <LoadingPage />;
  // }

  return (
    <AuthContext.Provider
      value={{
        ...state,
        signIn,
        logout,
        signUp,
        getProfile,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
