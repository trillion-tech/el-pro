import React, { useState, createContext } from 'react';

interface AuthProviderProps {
  children: any;
  auth: any;
}

export const AuthContextProvider = createContext({});

export const SetAuthContextProvider = createContext({});

export const AuthProvider = ({ children, auth }: AuthProviderProps) => {
  const [authState, setAuthState] = useState({
    user: auth,
  });

  return (
    <AuthContextProvider.Provider value={authState}>
      <SetAuthContextProvider.Provider value={setAuthState}>{children}</SetAuthContextProvider.Provider>
    </AuthContextProvider.Provider>
  );
};

export default AuthProvider;
