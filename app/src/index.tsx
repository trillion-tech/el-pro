import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import './App.css';
// import './translation/i18n';
// import './styles/fonts.css';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
var logger = (function () {
  var oldConsoleLog: any = null;
  var pub: any = {};

  pub.enableLogger = function enableLogger() {
    if (oldConsoleLog == null) return;

    window['console']['log'] = oldConsoleLog;
  };

  pub.disableLogger = function disableLogger() {
    oldConsoleLog = console.log;
    window['console']['log'] = function () {};
  };

  return pub;
})();

if (process.env.REACT_APP_LOGGER_LEVEL === 'debug') {
  logger.enableLogger();
} else {
  logger.disableLogger();
}

const container = document.getElementById('root') as HTMLElement;

const root = createRoot(container);

root.render(<App />);
