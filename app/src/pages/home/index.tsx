import React, { useState, useEffect } from 'react';
import { Box } from '@mui/material';

import { ILessonPagination } from 'src/interfaces/lesson';

import LessonTable from 'src/components/organisms/lesson-table';
import Container from 'src/components/molecules/container';

import style from './style';

const Home = () => {
  const [data, setData] = useState<ILessonPagination>({
    currentPage: 1,
    total: 0,
    items: [],
  });

  const loadLessonList = () => {
    // call api to get lesson list
    setData({
      currentPage: 1,
      total: 5,
      items: [
        {
          id: 1,
          name: 'Bai Test 1',
        },
        {
          id: 2,
          name: 'Bai Test 2',
        },
        {
          id: 3,
          name: 'Bai Test 3',
        },
        {
          id: 4,
          name: 'Bai Test 4',
        },
        {
          id: 5,
          name: 'Bai Test 5',
        },
        {
          id: 6,
          name: 'Bai Test 6',
        },
        {
          id: 7,
          name: 'Bai Test 7',
        },
        {
          id: 8,
          name: 'Bai Test 8',
        },
        {
          id: 9,
          name: 'Bai Test 9',
        },
        {
          id: 10,
          name: 'Bai Test 10',
        },
        {
          id: 11,
          name: 'Bai Test 11',
        },
        {
          id: 12,
          name: 'Bai Test 12',
        },
        {
          id: 13,
          name: 'Bai Test 13',
        },
      ],
    });
  };

  useEffect(() => {
    loadLessonList();
  }, []);

  return (
    <Container>
      <Box sx={style}>
        <Box className="title">Danh sách các bài Test</Box>
        <LessonTable data={data} />
      </Box>
    </Container>
  );
};

export default Home;
