import { COMMON_COLOR } from 'src/util/constants/color';

const style = {
  paddingTop: '15px',
  paddingBottom: '15px',
  '.back-button-box': {
    marginTop: '5px',
    marginBottom: '5px',
    '.back-button': {
      borderRadius: '5px',
    },
  },
  '.lesson-header-box': {
    padding: '15px 0',
    display: 'flex',
    height: '100px',
    '.info-box': {
      width: '78%',
      height: '100%',
      backgroundColor: '#D9D9D9',
      borderRadius: '15px',
      marginRight: '2%',
      '.image': {
        float: 'left',
        width: '100px',
        height: '100%',
        backgroundColor: 'white',
        textAlign: 'center',
        borderRadius: '15px 5px 5px 15px',
        display: 'flex',
        alignItem: 'center',
        justifyContent: 'center',
        img: {
          borderRadius: '15px 5px 5px 15px',
          width: '100%',
          height: '100%',
        },
      },
      '.description': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'start',
        height: '100%',
        '.content': {
          margin: '10px',
        },
        '.name': {
          fontSize: '30px',
          fontWeight: 'bold',
        },
      },
    },
    '.action-box': {
      width: '20%',
      height: '100%',
      fontSize: '20px',
      '*': {
        fontSize: '20px',
      },
      '.action-button': {
        borderRadius: '15px',
        '&.do-new': {
          borderColor: COMMON_COLOR.SUCCESS,
        },
        '&.repeat': {
          borderColor: COMMON_COLOR.ORANGE_F44,
        },
        height: '100%',
      },
    },
  },
  '.lesson-content-box': {
    '.content': {
      overflowY: 'scroll',
      overflowX: 'none',
      maxHeight: '94%',
      marginLeft: 'auto',
      marginRight: 'auto',
      display: 'inline-block',
      margin: '15px 10px 15px 10px',

      '.level-item': {
        float: 'left',
        width: '270px',
        height: '80px',
        margin: '10px',
        borderRadius: '10px',
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
    },
    backgroundColor: COMMON_COLOR.BASE,
    height: '388px',
    width: '100%',
    overflow: 'none',
    borderRadius: '10px',
    marginBottom: '15px',
    border: `1px solid ${COMMON_COLOR.GRAY}`,
  },
};

export default style;
