const style = {
  '.title': {
    fontSize: '40px',
    fontWeight: 'bold',
  },
  '.lesson-items-box': {
    '.item-row': {
      marginLeft: 'auto',
      marginRight: 'auto',
      display: 'inline-block',
      '.item': {
        float: 'left',
        width: '236px',
        height: '50px',
        fontSize: '25px',
        borderRadius: '10px',
        color: 'white',
        margin: '5px',
        textAlign: 'center',
        padding: '10px',
      },
    },
  },
};

export default style;
