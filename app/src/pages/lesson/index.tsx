import { useEffect, useState } from 'react';

import { useParams, useNavigate, generatePath } from 'react-router-dom';

// interfaces
import { ILessonInfo, ITestInfo } from 'src/interfaces/lesson';

// constants
import { COMMON_COLOR } from 'src/util/constants/color';

import { LESSON_TEST_LEVEL_PATH, DASHBOARD_PATH } from 'src/util/constants/path';
// components
import { Box } from '@mui/material';
import { BaseButton } from 'src/components/atoms/base-button';
import Container from 'src/components/molecules/container';
import Image from 'src/components/atoms/image';
import Loading from 'src/components/atoms/loading';

// styles
import style from './style';

const Lesson = () => {
  const { lessonId } = useParams();

  const navigate = useNavigate();
  const [isLoadingLesson, setLoadingLesson] = useState<boolean>(false);
  const [isLoadingTest, setLoadingTest] = useState<boolean>(false);

  const [lesson, setLesson] = useState<ILessonInfo | null>(null);
  const [testInfo, setTestInfo] = useState<Array<ITestInfo>>([]);

  const getLessonInfo = () => {
    setLoadingLesson(true);
    setTimeout(() => {
      setLoadingLesson(false);
      setLesson({
        id: Number(lessonId),
        name: 'Bài Test 1',
        content:
          '11 Lorem ipsum dolor sit amet, consectetuer adipiscingLorem ipsum dolor sit amet, consectetuer adipiscing elit. AenLorem ipsum dolor sit amet, consectetuer adipiscing elit. AenLorem ipsum dolor sit amet, consectetuer adipiscing elit. AenLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aen elit. Aenean color sit amet, consectetuer adipiscing elit. Aeneanolor sit amet, consectetuer adipiscing elit. Aeneanommodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,',
      });
    }, 1000);
  };

  const getTestInfo = () => {
    setLoadingTest(true);
    setTimeout(() => {
      setLoadingTest(false);
      setTestInfo([
        {
          id: 1,
          passed: false,
          answerNumber: 1,
          level: 1,
          answerTotal: 20,
          createAt: new Date().toISOString(),
        },
        {
          id: 2,
          passed: true,
          answerNumber: 20,
          level: 2,
          answerTotal: 20,
          createAt: new Date().toISOString(),
        },
      ]);
    }, 2000);
  };

  useEffect(() => {
    getLessonInfo();
    getTestInfo();
  }, [lessonId]);

  if (!lessonId) {
    return <></>;
  }

  const generateTestInfo = () => {
    if (!testInfo || !testInfo.length) {
      return <></>;
    }

    return (
      <Box className="test-info-box">
        {testInfo.map((item: ITestInfo) => {
          return (
            <Box
              key={item.id}
              sx={{
                color: item.passed ? COMMON_COLOR.SUCCESS : COMMON_COLOR.DANGER,
              }}
            >
              Kết quả: {item.answerNumber}/{item.answerTotal} - LEVEL: {item.level} - Ngày: {item.createAt}
            </Box>
          );
        })}
      </Box>
    );
  };

  const generateActionButton = () => {
    if (testInfo && testInfo.length) {
      return (
        <BaseButton
          content={'Làm lại'}
          variant={'warning'}
          className="action-button repeat"
          type="button"
          onClick={() => {
            navigate(generatePath(LESSON_TEST_LEVEL_PATH, { lessonId: lessonId?.toString() }));
          }}
        />
      );
    }
    return (
      <BaseButton
        content={'Làm bài'}
        variant={'success'}
        className="action-button do-new"
        type="button"
        onClick={() => {
          navigate(generatePath(LESSON_TEST_LEVEL_PATH, { lessonId: lessonId?.toString() }));
        }}
      />
    );
  };

  return (
    <>
      <Container>
        <Box sx={style}>
          <Box
            className="back-button-box"
            sx={{
              marginTop: '5px',
              marginBottom: '5px',
              '.back-button': {
                borderRadius: '5px',
              },
            }}
          >
            <BaseButton
              content={'Quay lại'}
              variant={'success'}
              type="button"
              className="back-button"
              height={'30px'}
              width={'150px'}
              fontSize={'15px'}
              onClick={() => {
                navigate(generatePath(DASHBOARD_PATH));
              }}
            />
          </Box>
          <Box className="lesson-header-box">
            <Box className="info-box">
              <Box className="image">
                <Image src={'/assets/images/default-lesson.png'} borderRadius="15px 5px 5px 15px" />
              </Box>
              <Box className="description">
                <Box className="content">
                  <Box className="name">{lesson?.name}</Box>
                  {generateTestInfo()}
                </Box>
              </Box>
            </Box>
            <Box className="action-box">{generateActionButton()}</Box>
          </Box>
          <Box className="lesson-content-box">
            <Box className="content">{lesson?.content}</Box>
          </Box>
        </Box>
      </Container>
      {(isLoadingLesson || isLoadingTest) && <Loading />}
    </>
  );
};

export default Lesson;
