import { COMMON_COLOR } from 'src/util/constants/color';
const style = {
  marginTop: '100px',

  '.signUp_Box': {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    '.box-input-username': {
      label: {
        fontSize: '18px',
        fontWeight: 'bold',
      },
    },
    '.box-input-password': {
      label: {
        fontSize: '18px',
        fontWeight: 'bold',
      },
    },
    '.box-button': {
      display: 'flex',
      marginTop: '10px',
    },
    '.login--title': {
      textAlign: 'center',
      fontSize: '60px',
      fontWeight: 'bold',
      color: COMMON_COLOR.GREEN_BLACK,
    },
    '.signUp-button': {
      marginRight: '10px',
      borderRadius: '10px',
      width: '200px',
      height: '59px',
      '*': {
        fontSize: '20px',
      },
    },
    '.signIN-button': {
      marginLeft: '10px',
      borderRadius: '10px',
      width: '183px',
      height: '59px',
      '*': {
        fontSize: '20px',
      },
    },
  },
};

export default style;
