import { useEffect, useState } from 'react';
import { useParams, useNavigate, generatePath } from 'react-router-dom';

// interfaces
import { ILessonInfo, ITestInfo } from 'src/interfaces/lesson';

// components
import Container from 'src/components/molecules/container';
import { Box } from '@mui/material';
import { BaseButton } from 'src/components/atoms/base-button';
import Image from 'src/components/atoms/image';
import { VTextField, useVForm, VFormContainer } from 'src/components/atoms/form';
import Loading from 'src/components/atoms/loading';

// constants
import { COMMON_COLOR } from 'src/util/constants/color';
import { LESSON_TEST_LEVEL_PATH } from 'src/util/constants/path';

//helpers
import { convertTimeToClockTime } from 'src/util/function';

//styles
import style from './style';

const LessonTest = () => {
  const { lessonId } = useParams();
  const formContext = useVForm();
  const navigate = useNavigate();
  const [isStart, setStart] = useState<boolean>(false);
  const [isInProcess, setInProcess] = useState<boolean>(false);

  const [isLoadingLesson, setLoadingLesson] = useState<boolean>(false);

  const [isLoadingResult, setLoadingResult] = useState<boolean>(false);

  const [processTime, setProcessTime] = useState<number>(0);
  const [processTimer, setProcessTimer] = useState<any>(null);
  const [isDisableProcess, setDisableProcess] = useState<boolean>(false);

  const [lesson, setLesson] = useState<ILessonInfo | null>({
    id: Number(lessonId),
    name: '',
    content: '',
    time: 0,
  });

  const [testContent, setTestContent] = useState<string>('');

  const [testInfo, setTestInfo] = useState<Array<ITestInfo>>([]);

  useEffect(
    () => {
      // console.log('useEffect isStart', isStart);

      // console.log('useEffect processTime', processTime);

      if (!isStart) {
        // console.log('clearTimeout processTimer', processTimer);
        clearTimeout(processTimer);
        return;
      }
      countTime(processTime, isStart);
      // this will clear Timeout
      // when component unmount like in willComponentUnmount
      // and show will not change to true
      return () => {};
    },
    // useEffect will run only one time with empty []
    // if you pass a value to array,
    // like this - [data]
    // than clearTimeout will run every time
    // this value changes (useEffect re-run)
    [isStart],
  );

  const getTestLessonInfo = () => {
    setLoadingLesson(true);
    setTimeout(() => {
      setLoadingLesson(false);
      const testData = {
        id: Number(lessonId),
        name: 'Bài Test 1',
        time: 120,
        content:
          '11 Lorem ipsum dolor sit ,xa#answerInput#sa, consectetuer adipiscingLorem ipsum dolor sit #answerInput#, consectetuer adipiscing elit. AenLorem ipsum dolor sit amet, consectetuer adipiscing elit. AenLorem ipsum dolor sit amet, consectetuer adipiscing elit. AenLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aen elit. Aenean color sit amet, consectetuer adipiscing elit. Aeneanolor sit amet, consectetuer adipiscing elit. Aeneanommodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt #answerInput#. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, #answerInput# velit cursus nunc,',
      };

      setLesson(testData);
      setTestContent(testData.content);
    }, 1000);
  };

  useEffect(() => {
    if (lessonId) {
      getTestLessonInfo();
    }
  }, [lessonId]);

  const countTime = (time: number, status: boolean) => {
    // console.log('startCountTime', time, status);
    const newTime = time - 1;

    if (newTime <= 0) {
      setDisableProcess(true);
    }

    // console.log('startCountTime newTime', newTime);

    const timer1 = setTimeout(() => {
      setProcessTime(newTime);
      if (status) {
        countTime(newTime, status);
      }
    }, 1000);

    setProcessTimer(timer1);

    if (!status || !time) {
      clearTimeout(processTimer);
      clearTimeout(timer1);
      return;
    }
  };

  if (!lessonId) {
    return <></>;
  }

  const generateTestInfo = () => {
    if (!testInfo || !testInfo.length) {
      return <></>;
    }

    return (
      <Box className="test-info-box">
        {testInfo.map((item: ITestInfo) => {
          return (
            <Box
              key={item.id}
              sx={{
                color: item.passed ? COMMON_COLOR.SUCCESS : COMMON_COLOR.DANGER,
              }}
            >
              Kết quả: {item.answerNumber}/{item.answerTotal} - LEVEL: {item.level}
            </Box>
          );
        })}
      </Box>
    );
  };

  const onBackLevelScreen = () => {
    navigate(generatePath(LESSON_TEST_LEVEL_PATH, { lessonId: lessonId?.toString() }));
    return;
  };

  const onStart = () => {
    // console.log('isStart && isInProcess', isStart, isInProcess);
    if (isStart && isInProcess) {
      setStart(false);
      return;
    }

    if (!isInProcess) {
      setProcessTime(lesson?.time || 0);
      setInProcess(true);
    }
    setStart(true);
  };

  const onCheckAnswer = (data: any) => {
    setLoadingResult(true);
    setTimeout(() => {
      setLoadingResult(false);
      setTestInfo([
        {
          id: 1,
          passed: true,
          answerNumber: 20,
          level: 1,
          answerTotal: 20,
          createAt: new Date().toISOString(),
        },
      ]);
    }, 1000);
  };

  const generateActionButton = () => {
    const timeClock = convertTimeToClockTime(isInProcess ? processTime : lesson?.time || 0);

    // console.log('timeClock', timeClock);

    const hours = timeClock.hours < 10 ? '0' + timeClock.hours : timeClock.hours;
    const minutes = timeClock.minutes < 10 ? '0' + timeClock.minutes : timeClock.minutes;
    const seconds = timeClock.seconds < 10 ? '0' + timeClock.seconds : timeClock.seconds;
    let isDisabled: boolean = isDisableProcess;
    let buttonBgColor: string = COMMON_COLOR.SUCCESS;
    let text: string = isStart ? 'Dừng lại' : 'Bắt đầu';

    if (isStart) {
      buttonBgColor = COMMON_COLOR.ORANGE_F44;
    } else {
      buttonBgColor = COMMON_COLOR.SUCCESS;
    }

    // console.log('processTime isInProcess isStart', processTime, isInProcess, isStart);

    if (processTime > 0 && isInProcess) {
      if (isStart) {
        text = 'Dừng lại';
      } else {
        text = 'Tiếp tục';
        buttonBgColor = COMMON_COLOR.SUCCESS;
      }

      if (processTime <= 10 && isStart) {
        buttonBgColor = COMMON_COLOR.DANGER;
      }
    }

    if (isInProcess && processTime <= 0) {
      text = 'Kết thúc';
      isDisabled = true;
      buttonBgColor = COMMON_COLOR.GRAY;
    }

    return (
      <BaseButton
        content={
          <Box>
            <Box>{text}</Box>
            <Box>
              {hours}:{minutes}:{seconds}
            </Box>
          </Box>
        }
        customStyles={{
          color: '#FFFFFF',
          backgroundColor: buttonBgColor,
          borderColor: buttonBgColor,
        }}
        disabled={isDisabled}
        className="action-button repeat"
        type="button"
        onClick={onStart}
      />
    );
  };

  const generateInputAnswers = () => {
    const contentArr: Array<string> = testContent.trim().split(' ');

    let answerIndex: number = 0;

    let newContent: any = null;

    for (const contentItem of contentArr) {
      console.log(
        'contentItem',
        contentItem,
        contentItem.includes('#answerInput#'),
        contentItem.indexOf('#answerInput#'),
        contentItem.length,
      );

      if (contentItem.includes('#answerInput#')) {
        const startIndex = contentItem.indexOf('#answerInput#');
        const endIndex = startIndex + 13;
        console.log('current character', contentItem.slice(startIndex, endIndex));
        console.log('start character', contentItem.slice(0, startIndex));
        console.log('end character', contentItem.slice(endIndex, contentItem.length));
        if (contentItem === '#answerInput#') {
          newContent = (
            <>
              {newContent}
              {
                <Box className="answer-input content-item">
                  <VTextField
                    label={''}
                    type="text"
                    name={`answerInput-${answerIndex}`}
                    placeholder={''}
                    showRequiredText={false}
                    disabled={isDisableProcess}
                    sx={{
                      backgroundColor: isDisableProcess ? COMMON_COLOR.GRAY : COMMON_COLOR.BASE,
                      input: {
                        backgroundColor: isDisableProcess ? COMMON_COLOR.GRAY : COMMON_COLOR.BASE,
                      },
                    }}
                  />
                </Box>
              }
            </>
          );
        } else {
          console.log('startCharacter answerInput', contentItem);
          const startIndex = contentItem.indexOf('#answerInput#');
          const endIndex = startIndex + 13;
          const startCharacter = startIndex === 0 ? '' : contentItem.slice(startIndex, endIndex);
          const endCharacter = endIndex === contentItem.length ? '' : contentItem.slice(endIndex, contentItem.length);

          console.log('startCharacter', startCharacter);
          console.log('endCharacter', startCharacter);

          newContent = (
            <>
              {newContent}
              {startCharacter}
              {
                <Box className="answer-input content-item">
                  <VTextField
                    label={''}
                    type="text"
                    name={`answerInput-${answerIndex}`}
                    placeholder={''}
                    showRequiredText={false}
                    disabled={isDisableProcess}
                    sx={{
                      backgroundColor: isDisableProcess ? COMMON_COLOR.GRAY : COMMON_COLOR.BASE,
                      input: {
                        backgroundColor: isDisableProcess ? COMMON_COLOR.GRAY : COMMON_COLOR.BASE,
                      },
                    }}
                  />
                </Box>
              }
              {endCharacter}
            </>
          );
        }

        answerIndex++;
      } else {
        newContent = (
          <>
            {newContent} <span className="text-item content-item">{contentItem} &ensp;</span>
          </>
        );
      }
    }

    return newContent;
  };

  return (
    <>
      <Container>
        <Box sx={style}>
          <Box
            className="back-button-box"
            sx={{
              marginTop: '5px',
              marginBottom: '5px',
              '.back-button': {
                borderRadius: '5px',
              },
            }}
          >
            <BaseButton
              content={'Quay lại'}
              variant={'success'}
              type="button"
              className="back-button"
              height={'30px'}
              width={'150px'}
              fontSize={'15px'}
              onClick={onBackLevelScreen}
            />
          </Box>
          <Box className="lesson-header-box">
            <Box className="info-box">
              <Box className="image">
                <Image src={'/assets/images/default-lesson.png'} borderRadius="15px 5px 5px 15px" />
              </Box>
              <Box className="description">
                <Box className="content">
                  <Box className="name">{lesson?.name}</Box>
                  {generateTestInfo()}
                </Box>
              </Box>
            </Box>
            <Box className="action-box">{generateActionButton()}</Box>
          </Box>
          <VFormContainer onSuccess={onCheckAnswer} formContext={formContext}>
            <Box className="process-action-box">
              <Box className="action-box">
                <BaseButton
                  content={'Kiểm tra'}
                  variant={'primary'}
                  className="action-button do-new"
                  type="submit"
                  customStyles={{
                    marginLeft: '5px',
                  }}
                />
              </Box>
            </Box>
            <Box className="lesson-content-box">
              <Box className="content">{generateInputAnswers()}</Box>
            </Box>
          </VFormContainer>
        </Box>
      </Container>
      {(isLoadingLesson || isLoadingResult) && <Loading />}
    </>
  );
};

export default LessonTest;
