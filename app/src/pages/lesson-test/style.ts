import { COMMON_COLOR } from 'src/util/constants/color';

const style = {
  paddingTop: '15px',
  paddingBottom: '15px',
  '.lesson-header-box': {
    padding: '15px 0',
    display: 'flex',
    height: '100px',
    '.info-box': {
      width: '78%',
      height: '100%',
      backgroundColor: '#D9D9D9',
      borderRadius: '15px',
      marginRight: '2%',
      '.image': {
        float: 'left',
        width: '100px',
        height: '100%',
        backgroundColor: 'white',
        textAlign: 'center',
        borderRadius: '15px 5px 5px 15px',
        display: 'flex',
        alignItem: 'center',
        justifyContent: 'center',
        img: {
          borderRadius: '15px 5px 5px 15px',
          width: '100%',
          height: '100%',
        },
      },
      '.description': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'start',
        height: '100%',
        '.content': {
          margin: '10px',
        },
        '.name': {
          fontSize: '30px',
          fontWeight: 'bold',
        },
      },
    },
    '.action-box': {
      width: '20%',
      height: '100%',
      fontSize: '20px',
      '*': {
        fontSize: '20px',
      },
      '.action-button': {
        borderRadius: '15px',
        height: '100%',
      },
    },
  },
  '.process-action-box': {
    margin: '10px 0',
    '.action-box': {
      display: 'flex',
      height: '100%',
      fontSize: '20px',
      '*': {
        fontSize: '20px',
      },
      '.action-button': {
        borderRadius: '15px',
        height: '50px',
        width: '150px',
      },
    },
  },
  '.lesson-content-box': {
    '.content': {
      width: '100%',
      overflowY: 'scroll',
      overflowX: 'none',
      height: '100%',
      '.answer-input': {
        border: 'unset',
        width: '100px',
        height: '30px',
        marginLeft: '5px',
        marginRight: '5px',
        display: '-webkit-inline-box',
        div: {
          width: '100%',
          height: '100%',
          margin: 0,
          padding: 0,
          border: 0,
        },
        input: {
          padding: '5px',
          fontSize: '15px',
          border: 0,
        },
        fieldset: {
          borderTop: 0,
          borderLeft: 0,
          borderRight: 0,
          borderRadius: 0,
          borderColor: COMMON_COLOR.BOLD_PRIMARY,
        },
      },
      '.text-item': {
        width: 'fit-content',
        height: '30px',
        margin: 0,
        alignItems: 'center',
        display: 'flex',
      },
      '.content-item': {
        float: 'left',
      },
    },
    backgroundColor: COMMON_COLOR.BASE,
    height: '388px',
    width: '100%',
    overflow: 'none',
    padding: '15px 10px 15px 10px',
    borderRadius: '10px',
    marginBottom: '15px',
    border: `1px solid ${COMMON_COLOR.GRAY}`,
  },
};

export default style;
