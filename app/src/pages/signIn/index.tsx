import React, { useState } from 'react';
import { Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import useAuth from 'src/hooks/useAuth';

import { VFormContainer, VTextField, useVForm, VPassword } from 'src/components/atoms/form';
import Loading from 'src/components/atoms/loading';
import { BaseMessage } from 'src/components/molecules/base-message';
import { IFormUserSignIn, MessageProps, TypeMessage } from 'src/interfaces/form';
import { BaseButton } from 'src/components/atoms/base-button';
import Container from 'src/components/molecules/container';

import { SIGN_UP } from 'src/util/constants/path';

import style from './style';

const SignIn = () => {
  const formContext = useVForm();

  const [loading, setLoading] = useState<boolean>(false);
  const [message, setMessage] = useState<MessageProps>({
    show: false,
    type: TypeMessage.success,
    message: '',
  });

  const navigate = useNavigate();

  const redirectSignUp = () => {
    navigate(SIGN_UP);
  };
  const { signIn } = useAuth();

  const onSubmit = async (userFormInfo: IFormUserSignIn) => {
    setLoading(true);
    try {
      const response: any = await signIn(userFormInfo);
      if (!response.success) {
        setMessage({
          show: true,
          type: TypeMessage.error,
          message: 'Đăng nhập thất bại',
        });
      } else {
        window.location.reload();
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Container>
        <Box sx={style}>
          <Box className="signIn_Box">
            <VFormContainer onSuccess={onSubmit} formContext={formContext}>
              <Box className="login--title">EL PRO</Box>
              <Box className="login-form-center">
                <Box>
                  <Box className="form-content">
                    <Box className="form-group box-input-username">
                      <VTextField
                        label={'Tài khoản'}
                        type="text"
                        name="username"
                        placeholder={'Nhập tài khoản...'}
                        required
                        validation={{
                          required: 'Tài khoản không được để trống',
                          maxLength: 255,
                        }}
                        showRequiredText={false}
                        hideRequiredLabel={true}
                      />
                    </Box>
                    <Box className="form-group box-input-password">
                      <VPassword
                        label={'Mật khẩu'}
                        name="password"
                        placeholder={'Nhập mật khẩu...'}
                        required
                        validation={{
                          required: 'Tài khoản không được để trống',
                          maxLength: 255,
                        }}
                        showRequiredText={false}
                        hideRequiredLabel={true}
                      />
                      <BaseMessage showMessage={message.show} type={message.type} message={message.message} />
                    </Box>
                    <Box className="box-button">
                      <BaseButton content={'Đăng nhập'} variant={'primary'} className="signIn-button" />
                      <BaseButton
                        content={'Đăng kí'}
                        variant={'success'}
                        className="signUp-button"
                        type="button"
                        onClick={redirectSignUp}
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            </VFormContainer>
          </Box>
        </Box>
      </Container>
      {loading && <Loading />}
    </>
  );
};

export default SignIn;
