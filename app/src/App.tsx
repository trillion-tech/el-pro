import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import {
  DASHBOARD_PATH,
  SIGN_IN,
  SIGN_UP,
  LESSON_DETAIL_PATH,
  MY_LESSON_PATH,
  LESSON_TEST_LEVEL_PATH,
  LESSON_TEST_PATH,
} from 'src/util/constants/path';

// auth component
import { AuthProvider, getSession } from 'src/contexts/auth-context';
//page components
import Home from 'src/pages/home';
import SignIn from 'src/pages/signIn';
import SignUp from 'src/pages/signUp';
import Lesson from 'src/pages/lesson';
import LessonLevel from 'src/pages/lesson-level';
import LessonTest from 'src/pages/lesson-test';
import MyLesson from 'src/pages/my-lesson';

const AppRouteComponent = () => {
  const routers = createBrowserRouter(
    getSession()
      ? [
          {
            path: DASHBOARD_PATH,
            element: <Home />,
          },
          {
            path: LESSON_DETAIL_PATH,
            element: <Lesson />,
          },
          {
            path: MY_LESSON_PATH,
            element: <MyLesson />,
          },
          {
            path: LESSON_TEST_LEVEL_PATH,
            element: <LessonLevel />,
          },
          {
            path: LESSON_TEST_PATH,
            element: <LessonTest />,
          },
        ]
      : [
          // sign in
          {
            path: SIGN_IN,
            element: <SignIn />,
          },
          {
            path: SIGN_UP,
            element: <SignUp />,
          },
        ],
  );

  return <RouterProvider router={routers} />;
};
const App = () => {
  console.log('App loading');
  return (
    <React.Fragment>
      <AuthProvider>
        <AppRouteComponent />
      </AuthProvider>
    </React.Fragment>
  );
};

export default App;
