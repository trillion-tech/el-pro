export interface IFormUserSignUp {
  username: string;
  password: string;
}

export interface IFormUserSignIn {
  username: string;
  password: string;
}

export interface MessageProps {
  show: boolean;
  type: TypeMessage.success | TypeMessage.error;
  message?: string;
}

export enum TypeMessage {
  error = 'error',
  warning = 'warning',
  success = 'success',
}
