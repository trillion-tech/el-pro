export interface ILessonInfo {
  id: number;
  name: string;
  content?: string;
  time?: number;
}

export interface ILessonPagination {
  currentPage: number;
  total: number;
  items: Array<ILessonInfo>;
}

export interface ITestInfo {
  id: number;
  passed: boolean;
  answerNumber: number;
  level: number;
  answerTotal: number;
  createAt?: string;
}

export interface ILessonLevel {
  id: number;
  name: string;
  color?: string;
}
