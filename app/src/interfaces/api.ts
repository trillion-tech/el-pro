import { AxiosError, AxiosResponse } from 'axios';

export interface ListResponse<T> {
  data: T[];
  total: number;
  last_page: number;
  success: true;
}
export interface ObjectResponse<T> {
  data: T;
  status?: number;
  success: true;
}

export interface ErrorResponse {
  success: false;
  message: string;
}

export interface ServerError {
  status?: number;
  message: string;
  // todo api error validate message
}
export type ApiResponse<T> = ObjectResponse<T> | ListResponse<T> | ErrorResponse;

export const responseError = (error: AxiosError | ServerError | any): ErrorResponse => {
  const { message } = error;
  return {
    message,
    success: false,
  };
};

export const responseObject = <T>(response: AxiosResponse | any): ObjectResponse<T> => {
  return {
    data: response.data.data ?? {},
    status: response.status,
    success: true,
  };
};

export const responseList = (response: AxiosResponse): ListResponse<any> => {
  return {
    data: response.data.data.items ?? [],
    total: response.data.data.total ?? 0,
    last_page: response.data.data.last_page ?? 0,
    success: true,
  };
};
