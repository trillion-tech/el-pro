export const FORM_WIDTH = '670px';

export const LABEL_WIDTH = '200px';

export const INPUT_STYLE = {
  '& .MuiInputBase-root': {
    borderRadius: '4px',
    minWidth: 'min(100%, 350px)',
    padding: '10px 13px',
    backgroundColor: '#FFFFFF',
    marginBottom: '10px',
  },
  '& .MuiOutlinedInput-notchedOutline': {
    border: '1px solid #D5DAE3',
  },
  '& .MuiInputBase-input': {
    padding: 0,
    fontSize: '14px',
  },
  '& .MuiFormHelperText-root': { margin: '3px 0 0' },
  '& .Mui-focused': {
    borderColor: '#003F73',
  },
};

export const LABEL_STYLE = {
  paddingTop: '10px',
  color: '#000000',
  whiteSpace: 'inherit',
  fontSize: '14px',
  fontWeight: 400,
  paddingRight: '10px',
};

export const FORM_GRID = {
  width: '100%',
  '& .formGridLabel': {
    width: '100%',
    maxWidth: { xs: '100%', sm: LABEL_WIDTH },
  },
  '& .formGridInput': {
    width: { xs: '100%', sm: '100%' },
    marginTop: { xs: '0px', sm: 0 },
  },
  '&.hasLabel': {
    '& > .formGridInput': {
      width: '100%',
    },
  },
};
