export const BASE_PATH = '';

// doasboard
export const DASHBOARD_PATH = `${BASE_PATH}/`;
export const SIGN_IN = `${BASE_PATH}/`;
export const SIGN_UP = `${BASE_PATH}/sign-up`;

export const LESSON_DETAIL_PATH = `${BASE_PATH}/lesson/:lessonId`;

export const MY_LESSON_PATH = `${BASE_PATH}/my-lesson`;
export const LESSON_TEST_LEVEL_PATH = `${BASE_PATH}/lesson-test/:lessonId`;

export const LESSON_TEST_PATH = `${BASE_PATH}/test/:lessonId/:levelId`;
