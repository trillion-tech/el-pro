import { getSession } from 'src/contexts/auth-context';
import axios, { AxiosRequestConfig } from 'axios';
import { makeUseAxios } from 'axios-hooks';

const baseURLAPI = process.env.MIX_APP_URL;
export const axiosDefault = axios.create({
  baseURL: baseURLAPI + '/',
  timeout: 120000,
  headers: {
    'Content-Type': 'application/json',
  },
});

axiosDefault.interceptors.request.use(async function (config) {
  const token = getSession();
  if (token) {
    config.headers = config.headers || {};
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export const useCustomAxios = makeUseAxios({
  axios: axiosDefault,
});

export const axiosConfig = async (): Promise<AxiosRequestConfig> => {
  return {
    baseURL: baseURLAPI,
    timeout: 60000,
    headers: {
      'Content-Type': 'application/json',
    },
  };
};
