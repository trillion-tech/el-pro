import DOMPurify from 'dompurify';

export const printf = (str: string, params: { [key: string]: string }) => {
  if (Object.keys(params).length) {
    for (const key in params) {
      str = str.replace(new RegExp('\\{' + key + '\\}', 'gi'), params[key]);
    }
  }
  return str;
};

export const printfMessageError = (str: string, params: { [key: string]: string }) => {
  if (Object.keys(params).length) {
    for (const key in params) {
      str = str.replace(new RegExp('\\{' + key + '\\}', 'gi'), params[key]);
    }
  }
  return str;
};

export const className = (args: { [key: string]: boolean } | string[] | string | number): string => {
  const classes: string[] = [];

  if (Array.isArray(args)) {
    if (args.length) {
      args.forEach((name) => {
        if (name) {
          classes.push(name);
        }
      });
    }
  } else if (typeof args === 'object') {
    Object.keys(args).forEach((name) => {
      if (args[name]) {
        classes.push(name);
      }
    });
  } else {
    const tmp = String(args);
    if (tmp) {
      classes.push(tmp);
    }
  }

  return classes.join(' ');
};

export const numberFormat = (value: number): string => {
  if (!value) return '';
  return value.toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&,');
};

export const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

export const sanitize = (text: string) => {
  if (!text) {
    return '';
  }

  const configs = {};
  return DOMPurify.sanitize(text, configs);
};

export const formatDate = (date: string) => {
  if (!date) return '';
  const newDate = new Date(date);
  return newDate.getDate() + 'Ngày' + (newDate.getMonth() + 1) + 'Tháng' + newDate.getFullYear() + 'Năm';
};

export const convertTimeToClockTime = (time: number) => {
  let hours: number = 0;
  let minutes: number = 0;

  if (time > 3600) {
    hours = parseInt((time / 3600).toString());
    time = time - hours * 3600;
  }

  if (time > 60) {
    minutes = parseInt((time / 60).toString());
    time = time - minutes * 60;
  }

  return {
    hours,
    minutes,
    seconds: time,
  };
};
