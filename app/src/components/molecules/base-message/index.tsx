import React, { useEffect, useState } from 'react';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { TypeMessage } from 'src/interfaces/form';

interface Props {
  showMessage?: boolean;
  type: TypeMessage.error | TypeMessage.warning | TypeMessage.success;
  message?: string;
  spacing?: number;
  width?: string;
}

export const BaseMessage = ({
  showMessage = false,
  type = TypeMessage.success,
  message = '成功',
  spacing = 2,
  width = '100%',
}: Props) => {
  const [showAlert, setShowAlert] = useState<boolean>(showMessage);

  useEffect(() => {
    setShowAlert(showMessage);
  }, [showMessage]);

  return (
    <>
      {showAlert && (
        <Stack
          sx={{
            width: width,
            margin: '10px 0px 10px 0px',
            '& .message': {
              fontSize: { xs: '1.6rem', md: '2rem' },
            },
          }}
          spacing={spacing}
        >
          {type == TypeMessage.error && (
            <Alert severity="error" className={'message'}>
              {message}
            </Alert>
          )}
          {type == TypeMessage.warning && (
            <Alert severity="warning" className={'message'}>
              {message}
            </Alert>
          )}
          {type == TypeMessage.success && (
            <Alert severity="success" className={'message'}>
              {message}
            </Alert>
          )}
        </Stack>
      )}
    </>
  );
};
