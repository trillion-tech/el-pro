import { Box } from '@mui/material';

const Container = ({ children }) => {
  return <Box sx={styles}>{children}</Box>;
};

const styles = {
  maxWidth: '900px',
  marginLeft: 'auto',
  marginRight: 'auto',
  height: '100%',
  position: 'relative',
};

export default Container;
