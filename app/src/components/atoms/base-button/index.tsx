import React from 'react';
import { Button, CircularProgress } from '@mui/material';
import { Box } from '@mui/system';
import { LinkProps } from 'react-router-dom';
import CustomLink from 'src/components/atoms/custom-link';
import { COMMON_COLOR } from 'src/util/constants/color';

export interface IBaseButton {
  content: string | JSX.Element | React.ReactElement;
  type?: 'submit' | 'button';
  variant?: 'primary' | 'white' | 'black' | 'tweet' | 'warning' | 'success' | 'gray';
  fullWidth?: boolean;
  borderRadius?: string;
  shadow?: boolean;
  height?: string;
  onClick?: (e: React.MouseEvent<HTMLElement>) => void;
  width?: string | { xs?: string; md?: string };
  icon?: React.ReactNode;
  iconPosition?: 'left' | 'right' | 'center' | 'none';
  fontSize?: string | { xs?: string; md?: string };
  className?: string;
  heightSp?: string;
  href?: LinkProps['to'];
  loading?: boolean;
  disabled?: boolean;
  classTextButton?: string;
  customStyles?: object;
}

export const BaseButton = ({
  content,
  type = 'submit',
  variant = 'primary',
  onClick,
  fullWidth,
  borderRadius = '3px',
  shadow,
  height = '48px',
  width = '240px',
  icon,
  iconPosition = 'none',
  fontSize = '2.0rem',
  heightSp = '40px',
  href = '',
  loading,
  disabled = false,
  classTextButton = '',
  customStyles = {},
  ...rest
}: IBaseButton): JSX.Element => {
  // default variant primary
  let backgroundColor = '#B14645';
  let color = '#FFF';
  let borderColor = '#B14645';

  if (variant === 'white') {
    backgroundColor = '#FFF';
    color = COMMON_COLOR.BASE;
    borderColor = COMMON_COLOR.BASE;
  }

  if (variant === 'black') {
    backgroundColor = '#1A1311';
    color = '#FFF';
    borderColor = '#1A1311';
  }

  if (variant === 'gray') {
    backgroundColor = '#707070';
    color = '#FFF';
    borderColor = '#707070';
  }

  if (variant === 'warning') {
    backgroundColor = COMMON_COLOR.ORANGE_F44;
    color = '#FFF';
    borderColor = COMMON_COLOR.ORANGE_F44;
  }

  if (variant === 'primary') {
    backgroundColor = COMMON_COLOR.PRIMARY;
    color = '#FFF';
    borderColor = COMMON_COLOR.PRIMARY;
  }

  if (variant === 'success') {
    backgroundColor = COMMON_COLOR.SUCCESS;
    color = '#FFF';
    borderColor = COMMON_COLOR.SUCCESS;
  }

  if (disabled) {
    backgroundColor = '#707070';
    color = '#FFF';
    borderColor = '#707070';
  }

  const styles = {
    display: 'flex',
    flexDirections: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: `${backgroundColor} `,
    color: `${color} `,
    borderColor: `${borderColor} `,
    width: fullWidth ? '100%' : width,
    maxWidth: '100%',
    borderRadius: borderRadius,
    boxShadow: shadow ? '0px 3px 20px #00000029' : 'none',
    height: { xs: `${heightSp} `, md: `${height} ` },
    border: '1px solid',
    padding: '0 16px',
    '& > div': {
      fontSize: { xs: '1.6rem', md: fontSize },
      fontWeight: 300,
    },
    '&:hover': {
      backgroundColor: `${backgroundColor} `,
      color: `${color} `,
      borderColor: `${borderColor} `,
      border: '1px solid',
      boxShadow: 'none',
      opacity: 0.9,
    },
    ...customStyles,
  };

  return (
    <Button
      sx={styles}
      onClick={(event: any) => {
        onClick && onClick(event);
      }}
      type={type}
      className={rest.className}
      disabled={disabled}
      {...rest}
    >
      {icon && iconPosition === 'left' ? icon : <Box style={{ width: '2px' }}></Box>}
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          fontSize: '11px',
        }}
      >
        {iconPosition === 'center' && (
          <React.Fragment>
            {icon}
            <span style={{ marginRight: '5px' }}></span>
          </React.Fragment>
        )}
        {href ? (
          <CustomLink href={href} color={color}>
            {content}
          </CustomLink>
        ) : (
          <Box className={classTextButton}>{content}</Box>
        )}
        {loading && <CircularProgress size={30} />}
      </Box>
      {icon && iconPosition === 'right' ? icon : <Box style={{ width: '2px' }}></Box>}
    </Button>
  );
};
