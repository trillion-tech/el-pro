import React, { useCallback, useState } from 'react';
import { Box } from '@mui/material';
import { LazyLoadImage } from 'react-lazy-load-image-component';

interface Props {
  alt?: string;
  src: string;
  height?: string | number | undefined;
  width?: string | number | undefined;
  caption?: string;
  placeholder?: React.ReactElement<any, string | React.JSXElementConstructor<any>> | null | undefined;
  borderRadius?: string | number | undefined;
  className?: string;
  objectFit?: string;
  layout?: string;
  style?: any;
  onClick?: () => void;
}
const Image = ({
  alt = '',
  src,
  height = '100%',
  width = '100%',
  placeholder,
  borderRadius = '0',
  className,
  objectFit,
  layout,
  style,
  onClick,
}: Props) => {
  const PlaceHolder = useCallback(() => {
    return (
      <Box
        sx={{
          width,
          height,
          backgroundColor: '#FAFAFA',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius,
        }}
      >
        {alt}
      </Box>
    );
  }, [width, height, alt, borderRadius]);

  const placeHolder = placeholder || <PlaceHolder />;
  const [isLoadSuccess, setIsLoadSuccess] = useState<boolean>(true);
  return (
    <React.Fragment>
      {!isLoadSuccess && placeHolder}
      {isLoadSuccess && (
        <LazyLoadImage
          onClick={onClick}
          style={{ ...style, borderRadius, objectFit, layout }}
          alt={alt}
          height={height}
          src={src}
          width={width}
          placeholder={placeHolder}
          onLoadedData={() => {
            setIsLoadSuccess(true);
          }}
          onError={() => {
            setIsLoadSuccess(false);
          }}
          className={className}
        />
      )}
    </React.Fragment>
  );
};

export default Image;
