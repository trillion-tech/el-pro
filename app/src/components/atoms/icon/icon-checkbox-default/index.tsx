import React from 'react';

interface Props {
  border?: string;
  color?: string;
}
export const IconCheckboxDefault = ({ border = '#003f73', color = '#e5e9fe' }: Props): JSX.Element => {
  return (
    <svg id="icon-select" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48">
      <g id="Rectangle_1014" data-name="Rectangle 1014" fill={color} stroke={border} strokeWidth="2">
        <rect width="48" height="48" rx="6" stroke="none" />
        <rect x="1" y="1" width="46" height="46" rx="5" fill="none" />
      </g>
    </svg>
  );
};
