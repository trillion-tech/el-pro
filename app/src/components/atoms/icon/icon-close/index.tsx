import React from 'react';

interface Props {
  width?: number;
  height?: number;
}
export const IconClose = ({ width = 15, height = 15 }: Props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 22 22">
      <path
        id="x001_-_E0A9"
        data-name="x001 - E0A9"
        d="M11.381-23.643l-.276-.276L1.381-14.2l-9.724-9.724-.276.276L1.1-13.919-8.619-4.2l.276.276,9.724-9.724L11.1-3.919l.276-.276L1.657-13.919Z"
        transform="translate(9.619 24.919)"
        fill="none"
        stroke="#0B3F73"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
      />
    </svg>
  );
};
