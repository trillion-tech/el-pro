import React from 'react';

interface Props {
  color?: string;
}
export const IconBack = ({ color = '#000' }: Props): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="8" height="13" viewBox="0 0 15.33 26">
      <path
        id="right001_-_E012"
        data-name="right001 - E012"
        d="M-7.378-17.5-9.72-19.842.95-30.512-9.72-41.158-7.378-43.5,5.61-30.512Z"
        transform="translate(5.61 -17.5) rotate(180)"
        fill={color}
      />
    </svg>
  );
};
