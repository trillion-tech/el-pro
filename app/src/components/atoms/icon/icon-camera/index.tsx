import React from 'react';

interface Props {
  width?: number;
  height?: number;
}
export const IconCamera = ({ width, height }: Props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 46.703 40.99">
      <g id="Group_3125" data-name="Group 3125" transform="translate(-103.474 -592.492)">
        <g id="Group_1254" data-name="Group 1254" transform="translate(103.974 592.992)">
          <path
            id="Path_537"
            data-name="Path 537"
            d="M45.418,13.141H39.086l-3.037-5.06A4.3,4.3,0,0,0,32.375,6H21.328a4.3,4.3,0,0,0-3.674,2.08l-3.037,5.061H8.285A4.289,4.289,0,0,0,4,17.426v24.28A4.289,4.289,0,0,0,8.285,45.99H45.418A4.289,4.289,0,0,0,49.7,41.706V17.426a4.289,4.289,0,0,0-4.285-4.285Zm1.428,28.564a1.43,1.43,0,0,1-1.428,1.428H8.285a1.43,1.43,0,0,1-1.428-1.428V17.426A1.43,1.43,0,0,1,8.285,16h7.141A1.428,1.428,0,0,0,16.65,15.3L20.1,9.55a1.436,1.436,0,0,1,1.225-.693H32.375A1.435,1.435,0,0,1,33.6,9.55L37.053,15.3A1.428,1.428,0,0,0,38.277,16h7.141a1.43,1.43,0,0,1,1.428,1.428Z"
            transform="translate(-4 -6)"
            fill="#003F73"
            stroke="#003F73"
            strokeWidth="1"
          />
          <path
            id="Path_538"
            data-name="Path 538"
            d="M22.429,14a9.929,9.929,0,1,0,9.929,9.929A9.929,9.929,0,0,0,22.429,14Zm0,17.21a7.281,7.281,0,1,1,7.281-7.281A7.281,7.281,0,0,1,22.429,31.21Z"
            transform="translate(0.423 -1.611)"
            fill="#003F73"
            stroke="#003F73"
            strokeWidth="1"
          />
        </g>
      </g>
    </svg>
  );
};
