import { Box } from '@mui/material';
import React, { useEffect, useState } from 'react';

interface props {
  active?: boolean;
  onClick?: (e: React.MouseEvent<HTMLElement>, status: boolean) => void;
  className?: string;
  activeIcon: React.ReactNode;
  inactiveIcon: React.ReactNode;
  disableOnClick?: boolean;
}

export const IconSwitchState = ({
  active = true,
  onClick,
  className = '',
  activeIcon,
  inactiveIcon,
  disableOnClick = false,
}: props): JSX.Element => {
  const [status, setStatus] = useState(active);
  useEffect(() => {
    setStatus(active);
  }, [active]);

  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    if (disableOnClick) {
      return;
    }
    const s = !status;
    setStatus(s);
    onClick && onClick(e, s);
  };

  return (
    <Box className={className} sx={{ display: 'flex', alignItems: 'center' }} onClick={handleClick}>
      {status ? activeIcon : inactiveIcon}
    </Box>
  );
};
