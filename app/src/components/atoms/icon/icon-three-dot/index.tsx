import React from 'react';

interface Props {
  size?: number;
}
export const IconThreeDot = ({ size = 4 }: Props): JSX.Element => {
  return (
    <svg
      id="_001-menu-1"
      data-name="001-menu-1"
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size * 4}
      viewBox="0 0 8 40"
    >
      <g id="Group_10" data-name="Group 10" transform="translate(0 0)">
        <g id="Group_9" data-name="Group 9">
          <circle id="Ellipse_6" data-name="Ellipse 6" cx="4" cy="4" r="4" transform="translate(0 16)" fill="#0b3f73" />
          <circle id="Ellipse_7" data-name="Ellipse 7" cx="4" cy="4" r="4" transform="translate(0 32)" fill="#0b3f73" />
          <circle id="Ellipse_8" data-name="Ellipse 8" cx="4" cy="4" r="4" fill="#0b3f73" />
        </g>
      </g>
    </svg>
  );
};
