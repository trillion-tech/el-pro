import React from 'react';

interface Props {
  size?: number;
}
export const IconSettings = ({ size = 24 }: Props): JSX.Element => {
  return <img id="設定アイコン" width={size} height={size} src="/images/patient-icon/icon-setting.png" />;
};
