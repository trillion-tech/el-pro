import React from 'react';

export const IconProfile = (): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 53 53">
      <g id="Group_2963" data-name="Group 2963" transform="translate(9343 23322)">
        <circle
          id="Ellipse_213"
          data-name="Ellipse 213"
          cx="26.5"
          cy="26.5"
          r="26.5"
          transform="translate(-9343 -23322)"
          fill="#fff"
        />
        <g id="Component_67_19" data-name="Component 67 – 19" transform="translate(-9331.386 -23312.879)">
          <path
            id="Path_1576"
            data-name="Path 1576"
            d="M-29.42-84.441a8.746,8.746,0,0,1-8.752,8.74,8.746,8.746,0,0,1-8.752-8.74,8.746,8.746,0,0,1,8.752-8.74,8.746,8.746,0,0,1,8.752,8.74Z"
            transform="translate(53.059 93.181)"
            fill="#0b3f73"
            stroke="rgba(0,0,0,0)"
            strokeLinejoin="round"
            strokeWidth="0"
          />
          <path
            id="Path_1577"
            data-name="Path 1577"
            d="M-58.014,9.286a12.5,12.5,0,0,1-6.91,2.095,12.5,12.5,0,0,1-6.913-2.094,9.844,9.844,0,0,0-7.973,9.671v5.976h29.771V18.958a9.844,9.844,0,0,0-7.975-9.672Z"
            transform="translate(79.811 9.825)"
            fill="#0b3f73"
            stroke="rgba(0,0,0,0)"
            strokeLinejoin="round"
            strokeWidth="0"
          />
        </g>
      </g>
    </svg>
  );
};
