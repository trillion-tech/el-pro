import React from 'react';

interface Props {
  width?: number;
  height?: number;
}
export const IconForwardNotification = ({ width, height }: Props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 16.745 27.415">
      <path
        id="right001_-_E012"
        data-name="right001 - E012"
        d="M-7.378-17.5-9.72-19.842.95-30.512-9.72-41.158-7.378-43.5,5.61-30.512Z"
        transform="translate(10.427 44.207)"
        fill="#4D4D4D"
        stroke="#707070"
        strokeWidth="1"
      />
    </svg>
  );
};
