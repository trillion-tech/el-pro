import React from 'react';

export const IconNotification = ({
  color = '#0b3f73',
  hasNewNoti = false,
}: {
  color?: string;
  hasNewNoti?: boolean;
}): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={20.5} height={21.5} viewBox="0 0 41.5 43.571">
      <g id="Group_3658" data-name="Group 3658" transform="translate(-684.5 -128)">
        <path
          id="Icon_awesome-bell"
          data-name="Icon awesome-bell"
          d="M17.75,40.571A5.071,5.071,0,0,0,22.819,35.5H12.681A5.071,5.071,0,0,0,17.75,40.571ZM34.818,28.708c-1.531-1.645-4.4-4.12-4.4-12.226a12.513,12.513,0,0,0-10.138-12.3V2.536a2.534,2.534,0,1,0-5.068,0V4.187a12.513,12.513,0,0,0-10.138,12.3c0,8.106-2.865,10.581-4.4,12.226A2.476,2.476,0,0,0,0,30.428a2.538,2.538,0,0,0,2.544,2.536H32.956A2.537,2.537,0,0,0,35.5,30.428,2.474,2.474,0,0,0,34.818,28.708Z"
          transform="translate(684.5 131)"
          fill={color}
        />
        {hasNewNoti && (
          <g id="Group_2477" data-name="Group 2477" transform="translate(651.711 33.789)">
            <circle
              id="Ellipse_203"
              data-name="Ellipse 203"
              cx="9.145"
              cy="9.145"
              r="9.145"
              transform="translate(56 94.211)"
              fill="red"
            />
          </g>
        )}
      </g>
    </svg>
  );
};
