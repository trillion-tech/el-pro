import React from 'react';
interface Props {
  color?: string;
}

export const IconPlusCircle = ({ color = '#fff' }: Props): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 40 40">
      <g id="Group_3015" data-name="Group 3015" transform="translate(-1911 -1865)">
        <g id="Group_2774" data-name="Group 2774" transform="translate(1418 499)">
          <path
            id="Icon_awesome-plus"
            data-name="Icon awesome-plus"
            d="M29.25,14.625H19.125V4.5a2.25,2.25,0,0,0-2.25-2.25h-2.25a2.25,2.25,0,0,0-2.25,2.25V14.625H2.25A2.25,2.25,0,0,0,0,16.875v2.25a2.25,2.25,0,0,0,2.25,2.25H12.375V31.5a2.25,2.25,0,0,0,2.25,2.25h2.25a2.25,2.25,0,0,0,2.25-2.25V21.375H29.25a2.25,2.25,0,0,0,2.25-2.25v-2.25A2.25,2.25,0,0,0,29.25,14.625Z"
            transform="translate(497 1368)"
            fill={color}
          />
          <g
            id="Ellipse_205"
            data-name="Ellipse 205"
            transform="translate(493 1366)"
            fill="none"
            stroke={color}
            strokeWidth="1"
          >
            <circle cx="20" cy="20" r="20" stroke="none" />
            <circle cx="20" cy="20" r="19.5" fill="none" />
          </g>
        </g>
      </g>
    </svg>
  );
};
