import React from 'react';

interface Props {
  size?: number;
  color?: string;
}
export const IconMinus = ({ size = 15, color = '#003f73' }: Props): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size} viewBox="0 0 31.113 31.113">
      <path
        id="x001_-_E0A9"
        data-name="x001 - E0A9"
        d="M11.381-23.643l-.276-.276L-8.619-4.2l.276.276Z"
        transform="translate(4.738 24.422) rotate(45)"
        fill="none"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
      />
    </svg>
  );
};
