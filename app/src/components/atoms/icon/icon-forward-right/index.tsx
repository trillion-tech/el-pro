import React from 'react';

export const IconForwardRight = ({ color = '#0b3f73', size = 14 }: { color?: string; size?: number }): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size * 0.6} viewBox="0 0 23.746 16.828">
      <g id="Group_2202" data-name="Group 2202" transform="translate(1 1.414)">
        <path
          id="line"
          d="M1.307,0H22.053"
          transform="translate(-1.307 6.949)"
          fill="none"
          stroke={color}
          strokeLinecap="round"
          strokeWidth="2"
        />
        <path
          id="path"
          d="M7.172,0,0,7l7.172,7"
          transform="translate(21.746 14) rotate(180)"
          fill="none"
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="2"
        />
      </g>
    </svg>
  );
};
