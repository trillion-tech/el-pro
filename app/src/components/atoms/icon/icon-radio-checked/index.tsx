import React from 'react';

export const IconRadioChecked = (): JSX.Element => {
  return (
    <svg id="icon-radio" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48">
      <g id="Ellipse_114" data-name="Ellipse 114" fill="#fff" stroke="#adadad" strokeWidth="2">
        <circle cx="24" cy="24" r="24" stroke="none" />
        <circle cx="24" cy="24" r="23" fill="none" />
      </g>
      <circle
        id="Ellipse_177"
        data-name="Ellipse 177"
        cx="10"
        cy="10"
        r="10"
        transform="translate(14 14)"
        fill="#da1f16"
      />
    </svg>
  );
};
