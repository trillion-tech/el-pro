import React from 'react';

interface Props {
  color?: string;
  size?: number;
}
export const IconCalendarHome = ({ size = 24 }: Props): JSX.Element => {
  return <img id="服用チェック" width={size} height={size / 1.1} src="/images/patient-icon/icon-taking-check.png" />;
};
