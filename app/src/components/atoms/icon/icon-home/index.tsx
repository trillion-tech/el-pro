import React from 'react';

interface Props {
  color?: string;
  size?: number;
}
export const IconHome = ({ color = '#adadad', size = 22 }: Props): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size} viewBox="0 0 43 47.557">
      <path
        id="Path_1419"
        data-name="Path 1419"
        d="M254.892,440.255l-20.5-17.21a1,1,0,0,0-1.286,0l-20.5,17.21a1,1,0,0,0-.357.766v28.348a1,1,0,0,0,1,1h12.907a1,1,0,0,0,1-1v-8.856a6.627,6.627,0,0,1,6.6-6.6h.01c.129,0,.259,0,.385.007a6.271,6.271,0,0,1,5.942,6.593v8.856a1,1,0,0,0,1,1h13.16a1,1,0,0,0,1-1V441.021A1,1,0,0,0,254.892,440.255Zm-21.143,12.656h0Zm19.5,15.456h-11.16v-7.808a8.262,8.262,0,0,0-7.847-8.639c-.166-.007-.33-.008-.5-.009a8.632,8.632,0,0,0-8.585,8.6v7.858H214.249V441.487l19.5-16.37,19.5,16.37Z"
        transform="translate(-212.249 -422.812)"
        fill={color}
      />
    </svg>
  );
};
