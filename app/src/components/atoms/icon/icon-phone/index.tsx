import React from 'react';

export const IconPhone = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14.33" height="14.33" viewBox="0 0 28.656 28.656">
      <path
        id="Icon_awesome-phone-alt"
        data-name="Icon awesome-phone-alt"
        d="M27.839,20.25,21.57,17.563A1.343,1.343,0,0,0,20,17.95l-2.776,3.392A20.746,20.746,0,0,1,7.31,11.424L10.7,8.648a1.34,1.34,0,0,0,.386-1.567L8.4.812A1.352,1.352,0,0,0,6.862.034L1.041,1.378A1.343,1.343,0,0,0,0,2.687a25.967,25.967,0,0,0,25.97,25.97,1.343,1.343,0,0,0,1.31-1.041l1.343-5.821a1.36,1.36,0,0,0-.784-1.545Z"
        transform="translate(0 0)"
        fill="#306AFA"
      />
    </svg>
  );
};
