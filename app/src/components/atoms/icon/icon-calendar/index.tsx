import React from 'react';

export const IconCalendar = (): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="15" viewBox="0 0 28 31.242">
      <g id="Group_3090" data-name="Group 3090" transform="translate(-40 -34)">
        <g id="calendar-icon" transform="translate(40.5 34.494)">
          <path
            id="path"
            d="M24.875,28.5H2.125A1.535,1.535,0,0,1,.5,26.875V4.125A1.535,1.535,0,0,1,2.125,2.5h22.75A1.535,1.535,0,0,1,26.5,4.125v22.75A1.535,1.535,0,0,1,24.875,28.5Z"
            transform="translate(0 1.248)"
            fill="none"
            stroke="#4d4d4d"
            strokeLinecap="round"
            strokeMiterlimit="10"
            strokeWidth="2"
          />
          <line
            id="line"
            y2="7.018"
            transform="translate(6.348 0.506)"
            fill="none"
            stroke="#4d4d4d"
            strokeLinecap="round"
            strokeMiterlimit="10"
            strokeWidth="2"
          />
          <line
            id="line-2"
            data-name="line"
            y2="7.018"
            transform="translate(20.554 0.506)"
            fill="none"
            stroke="#4d4d4d"
            strokeLinecap="round"
            strokeMiterlimit="10"
            strokeWidth="2"
          />
          <line
            id="line-3"
            data-name="line"
            x2="25.5"
            transform="translate(0.5 22.73)"
            fill="none"
            stroke="#4d4d4d"
            strokeLinecap="round"
            strokeMiterlimit="10"
            strokeWidth="2"
          />
        </g>
      </g>
    </svg>
  );
};
