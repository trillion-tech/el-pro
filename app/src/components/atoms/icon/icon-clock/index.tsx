import React from 'react';

interface Props {
  width?: number;
  height?: number;
}
export const IconClock = ({ width, height }: Props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 20.027 20.027">
      <g id="Group_3165" data-name="Group 3165" transform="translate(-30 -399.973)">
        <path
          id="Icon_material-access-time"
          data-name="Icon material-access-time"
          d="M13,3A10.013,10.013,0,1,0,23.027,13.013,10.009,10.009,0,0,0,13,3Zm.01,18.024a8.011,8.011,0,1,1,8.011-8.011A8.009,8.009,0,0,1,13.013,21.024Zm.5-13.017h-1.5v6.008l5.257,3.154.751-1.232-4.506-2.674Z"
          transform="translate(27 396.973)"
          fill="#ADADAD"
        />
      </g>
    </svg>
  );
};
