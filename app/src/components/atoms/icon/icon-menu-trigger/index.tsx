import React from 'react';

export const IconMenuTrigger = (): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="18" viewBox="0 0 50 36">
      <g id="Group_3014" data-name="Group 3014" transform="translate(-670 -37)">
        <g id="Group_2968" data-name="Group 2968" transform="translate(640 -10.814)">
          <rect
            id="Rectangle_1259"
            data-name="Rectangle 1259"
            width="50"
            height="3"
            rx="1.5"
            transform="translate(30 47.814)"
            fill="#0b3f73"
          />
          <rect
            id="Rectangle_1260"
            data-name="Rectangle 1260"
            width="50"
            height="3"
            rx="1.5"
            transform="translate(30 64.814)"
            fill="#0b3f73"
          />
          <rect
            id="Rectangle_1261"
            data-name="Rectangle 1261"
            width="50"
            height="3"
            rx="1.5"
            transform="translate(30 80.814)"
            fill="#0b3f73"
          />
        </g>
      </g>
    </svg>
  );
};
