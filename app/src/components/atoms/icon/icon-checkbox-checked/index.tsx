import React from 'react';

export const IconCheckboxChecked = (): JSX.Element => {
  return (
    <svg id="icon-select" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48">
      <rect id="Rectangle_1014" data-name="Rectangle 1014" width="48" height="48" rx="6" fill="#003f73" />
      <g id="パス_174" data-name="パス 174" transform="translate(-265.047 -391.504)">
        <path
          id="Path_255"
          data-name="Path 255"
          d="M286.87,421.907l-6.822-6.822,2.225-2.224,4.6,4.6,8.954-8.954,2.224,2.224Z"
          transform="translate(0 0)"
          fill="#fff"
        />
      </g>
    </svg>
  );
};
