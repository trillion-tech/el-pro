import React from 'react';

interface Props {
  size?: number;
  color?: string;
}
export const IconPlus = ({ size = 15, color = '#FFF' }: Props): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size} viewBox="0 0 31.113 31.113">
      <path
        id="x001_-_E0A9"
        data-name="x001 - E0A9"
        d="M11.381-23.643l-.276-.276L1.381-14.2l-9.724-9.724-.276.276L1.1-13.919-8.619-4.2l.276.276,9.724-9.724L11.1-3.919l.276-.276L1.657-13.919Z"
        transform="translate(4.738 24.422) rotate(45)"
        fill="none"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
      />
    </svg>
  );
};
