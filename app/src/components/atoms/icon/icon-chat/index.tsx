import React from 'react';

interface Props {
  size?: number;
}
export const IconChat = ({ size = 24 }: Props): JSX.Element => {
  return <img id="お薬相談" width={size} height={size} src="/images/patient-icon/icon-chat.png" />;
};
