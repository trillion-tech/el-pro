import React from 'react';

interface Props {
  border?: string;
  color?: string;
}
export const IconRadioDefault = ({ border = '#E6E6E6', color = '#FFF' }: Props): JSX.Element => {
  return (
    <svg id="icon-radio" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48">
      <g id="Ellipse_114" data-name="Ellipse 114" fill={color} stroke={border} strokeWidth="2">
        <circle cx="24" cy="24" r="24" stroke="none" />
        <circle cx="24" cy="24" r="23" fill="none" />
      </g>
    </svg>
  );
};
