import React from 'react';

interface Props {
  color?: string;
  size?: number;
}
export const IconDoctor = ({ size = 24 }: Props): JSX.Element => {
  return <img id="オンライン服薬指導" width={size} height={size * 1.17} src="/images/patient-icon/icon-doctor.png" />;
};
