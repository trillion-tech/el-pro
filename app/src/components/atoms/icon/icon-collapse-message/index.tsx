import React from 'react';

export const IconCollapseMessage = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 54 54">
      <g id="Group_3166" data-name="Group 3166" transform="translate(-32 -455)">
        <path
          id="Path_1461"
          data-name="Path 1461"
          d="M27,0A27,27,0,1,1,0,27,27,27,0,0,1,27,0Z"
          transform="translate(32 455)"
          fill="#003f73"
        />
        <g id="down-arrow" transform="translate(65.247 469) rotate(90)">
          <path
            id="Path_30"
            data-name="Path 30"
            d="M24.5.256a.875.875,0,0,0-1.237,0L12.387,11.151,1.492.256A.874.874,0,0,0,.256,1.492L11.748,12.984a.854.854,0,0,0,.618.256.89.89,0,0,0,.618-.256L24.476,1.492A.857.857,0,0,0,24.5.256Z"
            fill="#fff"
          />
        </g>
      </g>
    </svg>
  );
};
