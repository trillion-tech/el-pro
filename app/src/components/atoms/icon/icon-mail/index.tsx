import React from 'react';

interface Props {
  size?: number;
}
export const IconMail = ({ size = 24 }: Props): JSX.Element => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size * 0.71} viewBox="0 0 32.797 23.798">
      <g id="Group_3017" data-name="Group 3017" transform="translate(-517 -1027.15)">
        <g id="Group_2969" data-name="Group 2969" transform="translate(517 1027.15)">
          <g id="Group_3016" data-name="Group 3016">
            <path
              id="Path_1594"
              data-name="Path 1594"
              d="M219.8,159.021h13.692a4.594,4.594,0,0,1,1.2.1,1.966,1.966,0,0,1,1.451,1.633,6.769,6.769,0,0,1,.043.9q0,9.268,0,18.536a4.373,4.373,0,0,1-.091,1.123,1.965,1.965,0,0,1-1.635,1.453,6.614,6.614,0,0,1-.971.052H206a4.289,4.289,0,0,1-1.116-.1,1.96,1.96,0,0,1-1.458-1.655,6.841,6.841,0,0,1-.042-.833q0-9.3,0-18.59a4.024,4.024,0,0,1,.091-1.11,1.931,1.931,0,0,1,1.629-1.458,5.629,5.629,0,0,1,1.026-.052H219.8Z"
              transform="translate(-203.386 -159.021)"
              fill="#0b3f73"
              fillRule="evenodd"
            />
          </g>
        </g>
        <g id="Group_2970" data-name="Group 2970" transform="translate(518.163 1028.26)">
          <path
            id="Path_1595"
            data-name="Path 1595"
            d="M273.713,209.711H303.7c-.006.088-.068.114-.109.153q-3.535,3.36-7.072,6.719-3.846,3.653-7.689,7.309c-.09.085-.136.094-.231,0q-6.176-5.876-12.359-11.747l-2.413-2.292A.328.328,0,0,1,273.713,209.711Z"
            transform="translate(-273.479 -209.711)"
            fill="#fefefe"
            fillRule="evenodd"
          />
          <path
            id="Path_1596"
            data-name="Path 1596"
            d="M304.8,697.4H274.863c-.006-.073.046-.08.077-.1l10.363-7.335c.545-.385,1.091-.769,1.633-1.159a.125.125,0,0,1,.2.015c.859.823,1.724,1.64,2.583,2.463.089.085.135.088.226,0,.859-.823,1.724-1.641,2.583-2.463.072-.069.114-.076.2-.016q6,4.253,12.008,8.5c.026.018.05.039.075.059Z"
            transform="translate(-274.597 -675.804)"
            fill="#fefefe"
            fillRule="evenodd"
          />
          <path
            id="Path_1597"
            data-name="Path 1597"
            d="M265.465,272.154l-11.727,8.3V261.013Z"
            transform="translate(-253.738 -259.889)"
            fill="#fefefe"
            fillRule="evenodd"
          />
          <path
            id="Path_1598"
            data-name="Path 1598"
            d="M947.749,280.377l-11.73-8.3,11.73-11.144Z"
            transform="translate(-917.279 -259.812)"
            fill="#fefefe"
            fillRule="evenodd"
          />
        </g>
      </g>
    </svg>
  );
};
