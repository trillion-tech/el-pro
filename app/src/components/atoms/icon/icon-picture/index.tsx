import React from 'react';

interface Props {
  width?: number;
  height?: number;
}
export const IconPicture = ({ width, height }: Props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 44 44">
      <g id="Group_3127" data-name="Group 3127" transform="translate(-32 -591)">
        <g id="Group_3124" data-name="Group 3124">
          <rect
            id="Rectangle_619"
            data-name="Rectangle 619"
            width="40"
            height="40"
            rx="2"
            transform="translate(34 593)"
            fill="none"
            stroke="#003F73"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="4"
          />
        </g>
        <circle
          id="Ellipse_47"
          data-name="Ellipse 47"
          cx="2.999"
          cy="2.999"
          r="2.999"
          transform="translate(43.056 602.057)"
          fill="none"
          stroke="#003F73"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="3"
        />
        <path
          id="Path_143"
          data-name="Path 143"
          d="M40.791,19,30.92,10,4,30"
          transform="translate(31.991 600.988)"
          fill="none"
          stroke="#003F73"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="4"
        />
      </g>
    </svg>
  );
};
