import React from 'react';

interface Props {
  size?: number;
}
export const IconPrescriptions = ({ size = 24 }: Props): JSX.Element => {
  return <img id="お薬手帳" width={size} height={size * 1.25} src="/images/patient-icon/icon-medicine-notebook.png" />;
};
