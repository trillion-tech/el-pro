import React from 'react';

interface Props {
  width?: number;
  height?: number;
}
export const IconSendMessage = ({ width, height }: Props) => {
  return (
    <svg id="send" xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 31.992 29.704">
      <g id="Group_137" data-name="Group 137" transform="translate(0)">
        <path
          id="Path_80"
          data-name="Path 80"
          d="M31.33,29.834,1.623,16.123a1.149,1.149,0,0,0-1.366.32,1.136,1.136,0,0,0-.027,1.4L10,30.871.23,43.9a1.142,1.142,0,0,0,1.392,1.723L31.327,31.909a1.143,1.143,0,0,0,0-2.075Z"
          transform="translate(0 -16.02)"
          fill="#003F73"
        />
      </g>
    </svg>
  );
};
