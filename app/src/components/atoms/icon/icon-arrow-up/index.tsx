import React from 'react';

export const IconArrowUp = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="8" viewBox="0 0 26 15.33">
      <path
        id="right001_-_E012"
        data-name="right001 - E012"
        d="M-7.378-17.5-9.72-19.842.95-30.512-9.72-41.158-7.378-43.5,5.61-30.512Z"
        transform="translate(43.5 5.61) rotate(-90)"
        fill="#E6E6E6"
      />
    </svg>
  );
};
