import React from 'react';

interface Props {
  size?: number;
}
export const IconSendMail = ({ size = 24 }: Props): JSX.Element => {
  return (
    <img id="処方せん送信" width={size * 1.25} height={size} src="/images/patient-icon/icon-send-prescription.png" />
  );
};
