import { Box, CircularProgress } from '@mui/material';
import React from 'react';

const Loading = () => {
  return (
    <Box
      sx={{
        position: 'absolute',
        top: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%',
        backgroundColor: 'gray',
        opacity: 0.3,
        '& .MuiCircularProgress-root': {
          color: '#003F73',
          opacity: 1,
        },
      }}
    >
      <CircularProgress size={30} />
    </Box>
  );
};

export default Loading;
