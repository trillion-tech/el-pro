import React from 'react';
import { VTextField, VTextFieldProps } from 'src/components/atoms/form';
import { TextareaAutosize, TextareaAutosizeProps } from '@mui/material';

export type VTextAreaProps = VTextFieldProps & TextareaAutosizeProps & object;

export default function VTextArea({ maxRows, minRows, ...props }: VTextAreaProps) {
  return (
    <VTextField
      {...props}
      multiline
      InputProps={{
        ...props.InputProps,
        inputComponent: TextareaAutosize,
        inputProps: { maxRows, minRows },
      }}
    />
  );
}
