import React, { useState } from 'react';
import { VTextField, VTextFieldProps } from 'src/components/atoms/form';
import { IconButton, InputAdornment } from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

export type VPasswordProps = VTextFieldProps & object;

export default function PasswordElement({ ...props }: VPasswordProps): JSX.Element {
  const [password, setPassword] = useState<boolean>(true);
  const handleClickIcon = () => {
    setPassword(!password);
  };
  return (
    <VTextField
      {...props}
      type={password ? 'password' : 'text'}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton onClick={handleClickIcon} sx={{ color: '#000' }}>
              {password ? <VisibilityOffIcon /> : <VisibilityIcon />}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  );
}
