import React from 'react';
import { Box } from '@mui/material';

interface Props {
  text?: string;
  variantRequired?: 'red' | 'gray';
}
const RequiredLabel = ({ variantRequired = 'red', text = '必須' }: Props): JSX.Element => {
  let color = '#FFFFFF';
  let backgroundColor = '#DA1F16';

  if (variantRequired === 'gray') {
    text = '';
    color = '#666666';
    backgroundColor = '#FAFAFA';
  }

  const styles = {
    color: `${color} !important`,
    backgroundColor: `${backgroundColor} !important`,
    padding: '2px 9px',
    marginLeft: '6px',
    fontSize: '11px',
  };

  return (
    <Box component={'span'} sx={styles}>
      {text}
    </Box>
  );
};

export default RequiredLabel;
