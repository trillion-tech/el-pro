import React from 'react';
import { Box } from '@mui/material';
import { VTextField, VTextFieldProps } from 'src/components/atoms/form';

export type VSizeTextFieldProps = Omit<VTextFieldProps, 'name'> & {
  name1: string;
  name2: string;
  suffix1: string;
  suffix2: string;
  label: string;
};

const styles = {
  display: 'flex',
  alignItems: 'center',
  '& .vSizeTextFieldSpacing': {
    marginLeft: '10px',
  },
  '& .vSizeTextFieldInput': {
    maxWidth: '80px',
  },
};

export default function VSizeTextField({ label, name1, name2, suffix1, suffix2 }: VSizeTextFieldProps): JSX.Element {
  return (
    <Box sx={styles}>
      {label && <Box>{label}</Box>}
      <Box className={'vSizeTextFieldSpacing vSizeTextFieldInput'}>
        <VTextField name={name1} />
      </Box>
      {suffix1 && <Box className={'vSizeTextFieldSpacing'}>{suffix1}</Box>}
      <Box className={'vSizeTextFieldSpacing'}>~</Box>
      <Box className={'vSizeTextFieldSpacing vSizeTextFieldInput'}>
        <VTextField name={name2} />
      </Box>
      {suffix2 && <Box className={'vSizeTextFieldSpacing'}>{suffix2}</Box>}
    </Box>
  );
}
