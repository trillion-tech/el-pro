import React from 'react';
import { useController } from 'react-hook-form';
import { Checkbox, FormControlLabel, FormHelperText, Grid, InputLabel, RadioGroup } from '@mui/material';
import transformValidation from 'src/components/atoms/form/common/validation';
import { VCheckboxProps } from 'src/components/atoms/form';
import RequiredLabel from 'src/components/atoms/form/required-label';
import { FORM_GRID, LABEL_STYLE } from 'src/util/constants/form';
import { className } from 'src/util/function';
import { IconRadioDefault } from 'src/components/atoms/icon/icon-radio-default';
import { IconRadioChecked } from 'src/components/atoms/icon/icon-radio-checked';

export type VRadioGroupProps = VCheckboxProps & {
  options: any[];
  row?: boolean;
  labelKey?: string;
  valueKey?: string;
  returnObject?: boolean;
  onChange?: (value: any[]) => void;
  showRequiredText?: boolean;
};

export default function VRadioGroup({
  validation,
  helperText,
  options,
  label,
  errorLabel,
  name,
  parseError,
  required,
  labelKey = 'label',
  valueKey = 'id',
  returnObject,
  disabled,
  row,
  control,
  showRequiredText = true,
  ...rest
}: VRadioGroupProps): JSX.Element {
  // @ts-ignore
  validation = transformValidation(validation, errorLabel || label, required);

  const {
    field: { value = [], onChange },
    fieldState: { invalid, error },
  } = useController({
    name,
    rules: validation,
    control,
  });

  const handleChange = (index: number | string) => {
    const newArray = [...value];
    const exists = value.findIndex((i: any) => (returnObject ? i[valueKey] === index : i === index)) === -1;
    if (exists) {
      newArray.push(returnObject ? options.find((i) => i[valueKey] === index) : index);
    } else {
      newArray.splice(
        value.findIndex((i: any) => (returnObject ? i[valueKey] === index : i === index)),
        1,
      );
    }
    onChange(newArray);
    rest.onChange && rest.onChange(newArray);
  };

  return (
    <Grid container className={className({ hasLabel: !!label })} sx={FORM_GRID}>
      {label && (
        <Grid item className={'formGridLabel'}>
          <InputLabel error={!!error} sx={LABEL_STYLE}>
            {label} {required && showRequiredText ? <RequiredLabel /> : ''}
          </InputLabel>
        </Grid>
      )}
      <Grid item className={'formGridInput'}>
        <RadioGroup
          row={row}
          sx={{
            '& .MuiTypography-root': {
              fontSize: '14px !important',
            },
          }}
        >
          {options.map((option) => {
            const optionKey = option[valueKey];
            if (!optionKey) {
              console.error(`CheckboxButtonGroup: valueKey ${valueKey} does not exist on option`, option);
            }
            const isChecked =
              value.findIndex((item: any) => (returnObject ? item[valueKey] === optionKey : item === optionKey)) !== -1;
            return (
              <FormControlLabel
                control={
                  <Checkbox
                    icon={option.icon ? option.icon : <IconRadioDefault />}
                    checkedIcon={option.checkedIcon ? option.checkedIcon : <IconRadioChecked />}
                    sx={{
                      color: invalid ? 'error.main' : 'undefined',
                      '& .MuiSvgIcon-root': {
                        borderColor: '#999',
                      },
                      '&.Mui-checked': {
                        color: '#000',
                      },
                      '& svg': {
                        width: '25px',
                        height: '25px',
                      },
                    }}
                    value={optionKey}
                    checked={isChecked}
                    disabled={disabled}
                    onChange={() => handleChange(optionKey)}
                    className={className({ validateError: !!error, VCheckBox: true })}
                  />
                }
                label={option[labelKey]}
                key={optionKey}
              />
            );
          })}
        </RadioGroup>
        {helperText && <FormHelperText sx={{ margin: '3px 0 0' }}>{helperText}</FormHelperText>}
        {error && (
          <FormHelperText error={invalid} sx={{ margin: '3px 0 0' }}>
            {typeof parseError === 'function' ? parseError(error) : error.message}
          </FormHelperText>
        )}
      </Grid>
    </Grid>
  );
}
