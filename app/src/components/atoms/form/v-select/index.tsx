import React, { createElement, useEffect, useState } from 'react';
import {
  FormControlLabelProps,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  TextField,
  TextFieldProps,
} from '@mui/material';
import { Control, Controller, ControllerProps, FieldError } from 'react-hook-form';
import transformValidation from 'src/components/atoms/form/common/validation';
import RequiredLabel from 'src/components/atoms/form/required-label';
import { FORM_GRID, INPUT_STYLE, LABEL_STYLE } from 'src/util/constants/form';
import { className } from 'src/util/function';
import { IconArrowDown } from 'src/components/atoms/icon';

export type VSelectProps = Omit<TextFieldProps, 'name' | 'type' | 'onChange'> & {
  validation?: ControllerProps['rules'];
  name: string;
  options?: any[];
  valueKey?: string;
  labelKey?: string;
  type?: 'string' | 'number';
  parseError?: (error: FieldError) => string;
  objectOnChange?: boolean;
  onChange?: (value: any) => void;
  control?: Control<any>;
  onError?: (error: FieldError | undefined) => void;
  errorLabel?: FormControlLabelProps['label'];
  showRequiredText?: boolean;
  fullWidthOptions?: boolean;
  minHeightOptions?: string;
  notShowPlaceholderInOptions?: boolean;
};

export default function VSelect({
  name,
  required,
  valueKey = 'id',
  labelKey = 'label',
  options = [],
  parseError,
  type,
  objectOnChange,
  validation = {},
  control,
  onError,
  errorLabel,
  showRequiredText = true,
  label,
  placeholder,
  fullWidthOptions,
  minHeightOptions = '',
  notShowPlaceholderInOptions,
  ...rest
}: VSelectProps): JSX.Element {
  const isNativeSelect = !!rest.SelectProps?.native;
  const ChildComponent = isNativeSelect ? 'option' : MenuItem;
  // @ts-ignore
  validation = transformValidation(validation, errorLabel || label, required);
  const [isFocus, setIsFocus] = useState(false);

  return (
    <Controller
      name={name}
      rules={validation}
      control={control}
      render={({ field: { onBlur, onChange, value }, fieldState: { invalid, error } }) => {
        if (type === 'number' && value) {
          rest.InputLabelProps = rest.InputLabelProps || {};
          rest.InputLabelProps.shrink = true;
        }
        if (typeof value === 'object') {
          value = value[valueKey]; // if value is object get.md.md key
        }
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
          onError && onError(error);
        });
        return (
          <Grid container className={className({ hasLabel: !!label })} sx={FORM_GRID}>
            {label && (
              <Grid item className={'formGridLabel'}>
                <InputLabel focused={isFocus} sx={LABEL_STYLE}>
                  {label} {required && showRequiredText ? <RequiredLabel /> : ''}
                </InputLabel>
              </Grid>
            )}
            <Grid
              item
              className={'formGridInput'}
              sx={{
                '& .MuiInputBase-input': {
                  color: placeholder && (value === '999999' || !value) ? '#ADADAD' : '#000000',
                },
              }}
            >
              <TextField
                {...rest}
                label={''}
                name={name}
                placeholder={placeholder}
                value={placeholder ? value || '999999' : value || ''}
                onBlur={(e) => {
                  setIsFocus(false);
                  rest.onBlur && rest.onBlur(e);
                  return onBlur;
                }}
                onFocus={(e) => {
                  setIsFocus(true);
                  rest.onFocus && rest.onFocus(e);
                }}
                onChange={(event) => {
                  let item: number | string = event.target.value;
                  if (type === 'number') {
                    item = Number(item);
                  }
                  onChange(item);
                  if (typeof rest.onChange === 'function') {
                    if (objectOnChange) {
                      item = options.find((i: any) => i[valueKey] === item);
                    }
                    rest.onChange(item);
                  }
                }}
                className={className({ validateError: !!error, VSelect: true })}
                select
                required={required}
                error={invalid}
                sx={INPUT_STYLE}
                fullWidth
                SelectProps={{
                  IconComponent: () => <IconArrowDown />,
                  MenuProps: {
                    PaperProps: {
                      sx: {
                        '& .MuiPaper-root': {
                          minWidth: fullWidthOptions ? 'calc(100% - 32px) !important' : 'unset !important',
                          marginTop: fullWidthOptions ? '15px' : '',
                        },
                        minWidth: fullWidthOptions ? 'calc(100% - 32px) !important' : 'unset !important',
                        marginTop: fullWidthOptions ? '15px' : '',
                        '& .MuiList-root': {
                          padding: fullWidthOptions ? '0px !important' : '',
                          '&>li': {
                            borderBottom: fullWidthOptions ? '1px solid #EEEEEE' : '',
                            minHeight: minHeightOptions ? minHeightOptions : '',
                          },
                        },
                      },
                    },
                  },
                  renderValue: notShowPlaceholderInOptions
                    ? (data: any) =>
                        data !== '999999' ? options.find((item: any) => item[valueKey] === data)[labelKey] : placeholder
                    : undefined,
                }}
              >
                {isNativeSelect && <option />}
                {options.map((item: any) =>
                  createElement(
                    ChildComponent,
                    {
                      key: `${name}_${item[valueKey]}`,
                      value: item[valueKey],
                      disabled: item[valueKey] === '999999' ? true : false,
                    },
                    item[labelKey],
                  ),
                )}
              </TextField>
              {error && (
                <FormHelperText error={true} sx={{ marginTop: '-15px', fontSize: '10px' }}>
                  {typeof parseError === 'function' ? parseError(error) : error.message}
                </FormHelperText>
              )}
            </Grid>
          </Grid>
        );
      }}
    />
  );
}
