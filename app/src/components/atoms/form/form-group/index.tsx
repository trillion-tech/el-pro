import React, { ReactNode } from 'react';
import { Box, InputLabel } from '@mui/material';
import RequiredLabel from 'src/components/atoms/form/required-label';
import { className } from 'src/util/function';
import { LABEL_WIDTH } from 'src/util/constants/form';

type Props = {
  required?: boolean;
  label?: string;
  labelNote?: string;
  children: ReactNode;
  row?: boolean;
  error?: string | null;
};
const styles = {
  display: 'flex',
  marginTop: '15px',
  flexDirection: 'column',
  '&.hasLabel': {
    justifyContent: 'center',
    '& .formGroupInput': {
      width: { xs: '100%', sm: `calc(100% - ${LABEL_WIDTH})` },
      '& > *': {
        width: '100%',
      },
    },
  },
  '&.row': {
    flexDirection: { xs: 'column', sm: 'row' },
    '& .formGroupInput': {
      alignItems: { xs: 'flex-start', md: 'flex-start' },
    },
  },
  '& .row': { flexDirection: 'row' },
  '& .formGroupLabel': {
    display: 'flex',
    width: '100%',
    maxWidth: { xs: '100%', sm: LABEL_WIDTH },
    textAlign: 'left',
    flexDirection: 'row',
    flexWrap: 'wrap',
    fontSize: '14px',
    fontWeight: '300',
    '& .MuiFormLabel-root': {
      fontSize: '14px',
      color: '#666666',
      fontWeight: 'inherit',
      whiteSpace: 'inherit',
    },
  },
  '& .formGroupInput': {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginTop: { sm: 0, xs: '10px' },
    width: '100%',
    '& > *': {
      width: '100%',
    },
  },
  '& .formGroupLabelNote': { fontSize: '14px', fontWeight: 300 },
};

export const FormGroup = ({ required = false, label, labelNote, children, row = true, error }: Props): JSX.Element => {
  return (
    <Box sx={styles} className={className({ formGroup: true, row: !!row, hasLabel: !!label })}>
      {label && (
        <Box className={'formGroupLabel'}>
          <InputLabel>
            {label}
            {required && <RequiredLabel />}
            {labelNote && <span className={'formGroupLabelNote'}>{labelNote}</span>}
          </InputLabel>
        </Box>
      )}
      <Box className={'formGroupInput'}>{children}</Box>
      {error && <span style={{ color: '#d32f2f', fontSize: '13px !important', marginTop: '-5px' }}>{error}</span>}
    </Box>
  );
};
