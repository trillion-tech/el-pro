import React, { forwardRef, useEffect, useImperativeHandle } from 'react';
import { Box, FormControlLabelProps, FormHelperText, Typography } from '@mui/material';
import { Control, Controller, ControllerProps, FieldError, useFormContext } from 'react-hook-form';
import transformValidation from 'src/components/atoms/form/common/validation';
import useUniqueId from 'src/hooks/useUniqueId';
import { className } from 'src/util/function';

const styles = {
  display: 'inline-block',
  '& .flex': {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '18px',
  },
  '& .addIcon': { fontSize: '4rem' },
  '& .hidden': { display: 'none' },
  '& .boxWrapper': {
    margin: '0 auto',
    position: 'relative',
    '&.invalid': {
      borderColor: 'error.main',
    },
    '& video,img': {
      maxWidth: 'calc(100% - 2px)',
      maxHeight: 'calc(100% - 2px)',
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
    },
  },
  '& .label': {
    fontSize: '18px',
    color: '#ADADAD',
  },
  '& .title': {
    fontSize: '14px',
    marginBottom: '10px',
  },
  '& .box-button': {
    display: 'block',
    width: '153px',
    height: '40px',
  },
  '& .button-show-camera': {
    backgroundColor: '#003F73',
    borderRadius: '20px',
    textAlign: 'center',
    lineHeight: '40px',
    color: '#FFFFFF',
    marginTop: '10px',
    fontSize: '14px',
  },
};

export type VFileUploadRef = {
  removeFile: () => object;
};

export type VFileUploadProps = {
  validation?: ControllerProps['rules'];
  name: string;
  parseError?: (error: FieldError) => string;
  control?: Control<any>;
  onError?: (error: FieldError | undefined) => void;
  errorLabel?: FormControlLabelProps['label'];
  required?: boolean;
  preview?: boolean;
  previewRender?: (url: string, file: File) => object;
  previewUrl?: string;
  accept?: string;
  label?: string;
  width?: string;
  height?: string;
  title?: string;
  backgroundColor?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onDrop?: (event: React.DragEvent<HTMLElement>) => void;
  defaultBoxText?: string;
  btnLabel?: string;
  capture?: boolean;
};

function VFileUpload(
  {
    name,
    validation = {},
    parseError,
    required,
    control,
    preview = true,
    previewRender,
    previewUrl,
    accept = 'image/*',
    label = '作品画像を追加',
    width = '100%',
    height = '150px',
    backgroundColor = '#FAFAFA',
    title,
    defaultBoxText,
    btnLabel = '撮影する',
    capture = false,
    ...rest
  }: VFileUploadProps,
  ref: any,
) {
  // @ts-ignore
  validation = transformValidation(validation, rest.errorLabel || label, required);
  const { setValue, getValues } = useFormContext();
  const [url, setUrl] = React.useState(previewUrl);
  const [file, setFile] = React.useState<File | null>(null);
  const [labelText, setLabelText] = React.useState(defaultBoxText ?? '');
  const fileValue = getValues(name);

  const render = (file: File) => {
    if (preview) {
      url && URL.revokeObjectURL(url);
      setUrl(URL.createObjectURL(file));
    }
    setLabelText(file.name);
    setFile(file);
  };

  // update file state from formContext if rerender form
  if (!file && fileValue?.[0]) {
    render(fileValue[0]);
  }

  useEffect(() => {
    setLabelText(defaultBoxText ?? '');
  }, [defaultBoxText]);

  const id = useUniqueId('v-file-upload');

  function getFileType(file: File) {
    if (file.type.match('image.*')) return 'image';

    if (file.type.match('video.*')) return 'video';

    if (file.type.match('audio.*')) return 'audio';

    return 'other';
  }

  const renderDefaultBox = () => {
    return (
      <Box height={height} width={width} className={'flex'}>
        <Box textAlign={'center'}>
          <Typography className={'label'}>{labelText}</Typography>
        </Box>
      </Box>
    );
  };

  const renderPreview = (url: string, file: File) => {
    if (previewRender) {
      return previewRender(url, file);
    }
    const type = getFileType(file);
    // const defaultType = !(type === 'image' || type === 'video');
    return (
      <Box height={height} width={width} className={'flex'}>
        {type === 'image' && <img alt="file upload" src={url} />}
        {type === 'video' && <video src={url} />}
        {/* {defaultType && renderDefaultBox()} */}
      </Box>
    );
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event && event.target.files?.[0]) {
      render(event.target.files[0]);
      rest.onChange && rest.onChange(event);
    }
  };

  useImperativeHandle(ref, () => ({
    removeFile: () => {
      setValue(name, null);
      setUrl('');
      setFile(null);
      setLabelText(label);
    },
  }));
  return (
    <Controller
      name={name}
      control={control}
      rules={validation}
      render={({ field: { onChange, ref }, fieldState: { invalid, error } }) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
          rest.onError && rest.onError(error);
        });

        return (
          <>
            <Box sx={styles} width={width}>
              <Box className={'hidden'}>
                <input
                  onChange={(e) => {
                    handleChange(e);
                    const files = e.target.files;
                    return onChange(files);
                  }}
                  required={required}
                  accept={accept}
                  id={id}
                  type="file"
                  capture={capture ? 'environment' : false}
                  name={name}
                  ref={ref}
                />
              </Box>
              <Typography className={'title'}>{title}</Typography>
              <label htmlFor={id}>
                <Box
                  width={width}
                  height={height}
                  bgcolor={backgroundColor}
                  className={className({ boxWrapper: true, invalid: invalid, validateError: invalid })}
                >
                  <>
                    {preview && url && file
                      ? renderPreview(url, file)
                      : previewUrl && !file
                        ? null
                        : renderDefaultBox()}
                  </>
                  <>
                    {previewUrl && !file && (
                      <Box height={height} width={width} className={'flex'}>
                        <img alt="file upload" src={previewUrl} />
                      </Box>
                    )}
                  </>
                </Box>
              </label>
              {error && (
                <FormHelperText error={true}>
                  {typeof parseError === 'function' ? parseError(error) : error.message}
                </FormHelperText>
              )}
              <label htmlFor={id} className={'box-button'}>
                <Box className={'button-show-camera'}>{btnLabel}</Box>
              </label>
            </Box>
          </>
        );
      }}
    />
  );
}

export default forwardRef(VFileUpload);
