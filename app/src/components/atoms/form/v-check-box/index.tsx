import React, { useEffect } from 'react';
import { Control, Controller, ControllerProps, FieldError } from 'react-hook-form';
import {
  Checkbox,
  CheckboxProps,
  FormControl,
  FormControlLabel,
  FormControlLabelProps,
  FormGroup,
  FormHelperText,
} from '@mui/material';
import transformValidation from 'src/components/atoms/form/common/validation';
import { IconCheckboxDefault, IconCheckboxChecked } from 'src/components/atoms/icon';
export type VCheckboxProps = Omit<CheckboxProps, 'name'> & {
  validation?: ControllerProps['rules'];
  name: string;
  parseError?: (error: FieldError) => string;
  label?: FormControlLabelProps['label'];
  helperText?: string;
  control?: Control<any>;
  onError?: (error: FieldError | undefined) => void;
  errorLabel?: FormControlLabelProps['label'];
};

export default function VCheckbox({
  name,
  validation = {},
  required,
  parseError,
  label,
  errorLabel,
  onError,
  control,
  helperText,
  ...rest
}: VCheckboxProps): JSX.Element {
  // @ts-ignore
  validation = transformValidation(validation, errorLabel || label, required);

  return (
    <Controller
      name={name}
      rules={validation}
      control={control}
      render={({ field: { value, onChange }, fieldState: { invalid, error } }) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
          onError && onError(error);
        });
        return (
          <FormControl
            error={invalid}
            sx={{
              '& .MuiTypography-root': {
                fontSize: '14px !important',
              },
            }}
          >
            <FormGroup row>
              <FormControlLabel
                label={label || ''}
                control={
                  <Checkbox
                    disabled={rest.disabled}
                    icon={rest.icon ? rest.icon : <IconCheckboxDefault border={invalid ? '#E10000' : '#003f73'} />}
                    checkedIcon={rest.checkedIcon ? rest.checkedIcon : <IconCheckboxChecked />}
                    sx={{
                      color: invalid ? 'error.main' : 'undefined',
                      '& .MuiSvgIcon-root': {
                        borderColor: '#999',
                      },
                      '&.Mui-checked': {
                        color: '#000',
                      },
                      '& svg': {
                        width: '25px',
                        height: '25px',
                      },
                    }}
                    value={value}
                    checked={!!value}
                    onChange={(e) => {
                      onChange(!value);
                      rest.onChange && rest.onChange(e, !value);
                    }}
                  />
                }
              />
            </FormGroup>
            {helperText && <FormHelperText sx={{ margin: '3px 0 0' }}>{helperText}</FormHelperText>}
            {error && (
              <FormHelperText error={invalid} sx={{ margin: '-10px 0 0' }}>
                {typeof parseError === 'function' ? parseError(error) : error.message}
              </FormHelperText>
            )}
          </FormControl>
        );
      }}
    />
  );
}
