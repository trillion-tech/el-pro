import errorMessage from './message';

export const validateEmail = (email: string, label: any = 'Dữ liệu này') => {
  const message = errorMessage('email', label);

  return (
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      email?.toLowerCase(),
    ) || message
  );
};

export const validatePhone = (value: string, label: any = 'Dữ liệu này') => {
  const message = errorMessage('phone', label);

  return /^0\d{9,10}$/.test(value) || message;
};

export const requiredIf = (value: string, anotherFieldValue: string, label: any = 'Dữ liệu này') => {
  const valid = !!(value || !anotherFieldValue);
  return valid || errorMessage('requiredIf', label);
};

export const validateImage = (files: FileList | null, label: any = 'Dữ liệu này') => {
  if (!files || !files[0]) {
    return true;
  }
  const message = errorMessage('image', label);
  return files[0].type.includes('image') || message;
};

export const validateValueWithPlaceholder = (value: string, label: any = 'Dữ liệu này') => {
  const message = errorMessage('valueWithPlaceholder', label);
  if (!value || value === '999999' || value?.length === 0) return message;
  return true;
};
export const validateNotRequiredValueWithPlaceholder = (value: string, _label: any = 'Dữ liệu này') => {
  if (!value || value === '999999' || value?.length === 0) return true;
  return true;
};

export const validateZipcode = (value: string, label: any = 'Dữ liệu này') => {
  if (!value) {
    return true;
  }
  const message = errorMessage('default', label);
  const rule = /^\d{7}$/gm;
  return rule.test(value) || message;
};

export const validateSelectRequiredPrefecture = (value: any, _label: any = 'Dữ liệu này') => {
  if (!value || value.id === null) return 'Chọn tỉnh';
  return true;
};

export const validateSelectRequired = (value: string, _label: any = 'Dữ liệu này') => {
  if (!value || value === '999999' || value?.length === 0) return 'Vui lòng chọn';
  return true;
};

export const validateBirthday = (value: string) => {
  var currentDate = new Date();
  var valueDate = new Date(value);
  var minDate = new Date(`${currentDate.getFullYear() - 150}-01-01`);
  minDate.setHours(0, 0, 0, 0);
  currentDate.setHours(0, 0, 0, 0);
  valueDate.setHours(0, 0, 0, 0);
  if (valueDate >= currentDate) {
    return 'Vui lòng đặt ngày sinh của bạn trước ngày hôm nay.';
  }
  if (valueDate < minDate) {
    return 'Ngày sinh không hợp lệ';
  }
  return true;
};