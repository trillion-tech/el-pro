import { printfMessageError } from 'src/util/function';

const lang = {
  default: '{field} định dạng không hợp lệ',
  required: '{field} không được để trống',
  requiredIf: '{field} không được để trống',
  email: '{field} không đúng dạng email',
  phone: '{field} không đúng dạng số điện thoại',
  image: '{field} định dạng không hợp lệ',
  max: '{field} có kích thước tối đa {length} kí tự',
  min: '{field}có kích thước tối thiểu {length} kí tự',
  valueWithPlaceholder: 'Vui long nhập {field}',
};

const getMessage = (path: string, obj: Record<any, any>, defaultValue?: string) => {
  return path.split('.').reduce((o, p) => (o && o[p] ? o[p] : defaultValue), obj);
};

const errorMessage = (key: string, field: string | Record<string, string>) => {
  const message = getMessage(key, lang);
  if (typeof message !== 'string') {
    return printfMessageError(lang.default, typeof field === 'object' ? field : { field: field });
  }

  return printfMessageError(message, typeof field === 'object' ? field : { field: field });
};

export default errorMessage;
