import { useForm } from 'react-hook-form';
import { FieldValues, UseFormProps, UseFormReturn } from 'react-hook-form/dist/types';

export default function useVForm<TFieldValues extends FieldValues = FieldValues>(
  props?: UseFormProps<TFieldValues, object>,
): UseFormReturn<TFieldValues, object> {
  if (props) {
    props.mode = props.mode || 'all';
  }

  return useForm({ ...props });
}
