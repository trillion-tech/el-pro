import React from 'react';
import { VPassword, VPasswordProps } from 'src/components/atoms/form';
import { useWatch } from 'react-hook-form';

export type VPasswordConfirmProps = VPasswordProps & {
  passwordFieldName: string;
};

export default function VPasswordConfirm({ passwordFieldName, ...props }: VPasswordConfirmProps) {
  const pwValue = useWatch({
    name: passwordFieldName,
    control: props.control,
    defaultValue: '',
  });

  return (
    <VPassword
      {...props}
      validation={{
        validate: (value: string) => {
          return value === pwValue || ' パスワード設定が一致しません';
        },
      }}
    />
  );
}
