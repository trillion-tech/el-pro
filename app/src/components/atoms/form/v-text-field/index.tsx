import React, { useEffect, useState } from 'react';
import { Box, FormControlLabelProps, FormHelperText, Grid, InputLabel, TextField, TextFieldProps } from '@mui/material';
import { Control, Controller, ControllerProps, FieldError } from 'react-hook-form';
import transformValidation from 'src/components/atoms/form/common/validation';
import RequiredLabel from 'src/components/atoms/form/required-label';
import { FORM_GRID, INPUT_STYLE, LABEL_STYLE } from 'src/util/constants/form';
import { className } from 'src/util/function';

export type VTextFieldProps = Omit<TextFieldProps, 'name'> & {
  validation?: ControllerProps['rules'];
  name: string;
  parseError?: (error: FieldError) => string;
  control?: Control<any>;
  onError?: (error: FieldError | undefined) => void;
  errorLabel?: FormControlLabelProps['label'];
  showRequiredText?: boolean;
  description?: string;
  unitLabel?: string;
  hideRequiredLabel?: boolean;
  isHideError?: boolean;
  maxLengthForNumber?: number;
};

export default function VTextField({
  validation = {},
  parseError,
  type,
  required,
  name,
  control,
  showRequiredText = true,
  label,
  description,
  unitLabel,
  hideRequiredLabel = false,
  isHideError = false,
  maxLengthForNumber,
  ...rest
}: VTextFieldProps): JSX.Element {
  // @ts-ignore
  validation = transformValidation(validation, rest.errorLabel || label, required);
  const [isFocus, setIsFocus] = useState(false);
  return (
    <Controller
      name={name}
      control={control}
      rules={validation}
      render={({ field: { value, onChange, onBlur }, fieldState: { invalid, error } }) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
          rest.onError && rest.onError(error);
        });
        return (
          <Grid container className={className({ hasLabel: !!label })} sx={FORM_GRID}>
            {label && (
              <Grid item className={'formGridLabel'}>
                <InputLabel focused={isFocus} sx={LABEL_STYLE}>
                  {label}{' '}
                  {(required && showRequiredText && !hideRequiredLabel && <RequiredLabel variantRequired="red" />) ||
                    (required && !showRequiredText && !hideRequiredLabel && <RequiredLabel variantRequired="gray" />)}
                </InputLabel>
              </Grid>
            )}
            <Grid item className={'formGridInput'}>
              <Box
                sx={{
                  display: unitLabel ? 'flex' : 'block',
                  flexDirection: unitLabel ? 'row' : 'column',
                  alignItems: unitLabel ? 'center' : 'flex-start',
                }}
              >
                <TextField
                  {...rest}
                  label={''}
                  name={name}
                  value={value || ''}
                  onChange={(e) => {
                    rest.onChange && rest.onChange(e);
                    return onChange(e);
                  }}
                  onBlur={(e) => {
                    setIsFocus(false);
                    rest.onBlur && rest.onBlur(e);
                    return onBlur;
                  }}
                  onFocus={() => {
                    setIsFocus(true);
                  }}
                  required={required}
                  type={type}
                  error={invalid}
                  sx={INPUT_STYLE}
                  fullWidth
                  onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                    if (maxLengthForNumber && type === 'number') {
                      e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, maxLengthForNumber);
                    }
                  }}
                  className={className({ validateError: !!error, VTextField: true })}
                />
                {unitLabel && (
                  <Box
                    sx={{ fontSize: '14px', color: '#4D4D4D', width: '90px', marginBottom: '10px', marginLeft: '10px' }}
                  >
                    {unitLabel}
                  </Box>
                )}
              </Box>
              {error && !isHideError && (
                <FormHelperText
                  error={true}
                  sx={{
                    marginTop: '-5px',
                    '& .MuiFormHelperText-root': {
                      fontSize: '13px !important',
                    },
                  }}
                >
                  {typeof parseError === 'function' ? parseError(error) : error.message}
                </FormHelperText>
              )}
              {description && (
                <Box sx={{ fontSize: '10px', color: '#666666', marginTop: error ? '0px' : '-13px' }}>{description}</Box>
              )}
            </Grid>
          </Grid>
        );
      }}
    />
  );
}
