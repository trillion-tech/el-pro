import React, { ReactNode } from 'react';
import { Box } from '@mui/material';
import { className } from 'src/util/function';

type Props = {
  children: ReactNode;
  row?: boolean;
};
const styles = {
  marginTop: '30px',
  display: 'flex',
  flexDirection: 'column',
  gap: '10px',
  justifyContent: 'center',
  alignItems: 'center',
  '&.row': {
    flexDirection: { xs: 'column', sm: 'row' },
  },
};

export const FormGroupButton = ({ children, row = true }: Props): JSX.Element => {
  return (
    <Box sx={styles} className={className({ row: !!row })}>
      {children}
    </Box>
  );
};
