import React, { useMemo } from 'react';
import { Link, LinkProps } from 'react-router-dom';

export type CustomLinkProp = {
  href: LinkProps['to'];
  children: React.ReactNode;
  target?: '_blank' | '_self' | '_parent' | '_top' | 'framename' | '';
  // onClick?: (e: any) => void;
  className?: string;
  color?: string;
  textDecoration?: string;
};

const CustomLink = ({
  href,
  children,
  className,
  color = '#000',
  textDecoration = 'none',
  target = '',
}: CustomLinkProp) => {
  const style = useMemo(() => {
    return {
      color: color,
      textDecoration: textDecoration,
    };
  }, [color, textDecoration]);
  return (
    <Link to={href} className={className} target={target} style={style}>
      {children}
    </Link>
  );
};

export default CustomLink;
