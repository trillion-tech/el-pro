import React from 'react';
import { Box, Typography } from '@mui/material';
import { ILessonPagination, ILessonInfo } from 'src/interfaces/lesson';
import { COMMON_COLOR } from 'src/util/constants/color';
import { LESSON_DETAIL_PATH } from 'src/util/constants/path';
import { Link, generatePath } from 'react-router-dom';

import style from './style';

interface LessonTableProps {
  data: ILessonPagination;
}

const LessonTable = ({ data }: LessonTableProps) => {
  const getBG = (index: number): string => {
    if (index % 3 === 0) {
      return COMMON_COLOR.LIGHT_BROWN;
    }

    if (index % 3 === 2) {
      return COMMON_COLOR.BOLD_PRIMARY;
    }

    return COMMON_COLOR.SUCCESS;
  };

  return (
    <Box sx={style}>
      <Box className="lesson-items-box">
        <Box className="item-row">
          {data.items.map((item: ILessonInfo, index: number) => {
            return (
              <Link to={generatePath(LESSON_DETAIL_PATH, { lessonId: item.id.toString() })} key={item.id}>
                <Box
                  className="item"
                  sx={{
                    backgroundColor: getBG(index + 1),
                  }}
                >
                  <Typography className="lesson-name">{item.name}</Typography>
                </Box>
              </Link>
            );
          })}
        </Box>
      </Box>
    </Box>
  );
};

export default LessonTable;
